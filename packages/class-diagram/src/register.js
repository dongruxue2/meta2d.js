import { interfaceClass } from './interfaceClass';
import { simpleClass } from './simpleClass';
export function classPens() {
    return {
        interfaceClass: interfaceClass,
        simpleClass: simpleClass,
    };
}
//# sourceMappingURL=register.js.map