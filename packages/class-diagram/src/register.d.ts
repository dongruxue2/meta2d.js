import { Pen } from '@meta2d/core';
export declare function classPens(): Record<string, (pen: Pen, ctx?: CanvasRenderingContext2D) => Path2D>;
