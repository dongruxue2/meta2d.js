import { Pen } from '../../core/src/pen';
export declare function simpleClass(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
