import { Pen } from '../../core/src/pen';
export declare function interfaceClass(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
