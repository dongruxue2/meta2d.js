export const mode: string;
export const entry: string;
export namespace output {
    const path: string;
    const filename: string;
    const library: string;
    const libraryTarget: string;
}
export namespace module {
    const rules: {
        test: RegExp;
        use: string[];
        exclude: string[];
    }[];
}
export namespace resolve {
    const extensions: string[];
    const plugins: any[];
}
