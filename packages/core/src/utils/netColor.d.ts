export declare function getRandomColor(): {
    r: number;
    g: number;
    b: number;
};
export declare function stringGetLineCount(line: string): number;
export declare function clamp(v: number, min: number, max: number): number;
export declare function parseXml(xml: string): Document | null;
export declare function setInterpolatedColor(svg: any, targetColor: string): Document | null;
