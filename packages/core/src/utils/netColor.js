/*
 * Created by Administrator on 2016/10/19.
 */
"use strict";
export function getRandomColor() {
    return {
        r: Math.floor(Math.random() * 255),
        g: Math.floor(Math.random() * 255),
        b: Math.floor(Math.random() * 255)
    };
}
export function stringGetLineCount(line) {
    var num = 1;
    var length = line.length;
    for (var i = 0; i < length; i++) {
        if (line[i] == '\n') {
            num++;
        }
    }
    return num;
}
/*
 * 取中间值
 *@param Number v  - 目标值
*@param Number min  - 最小值
*@param Number max  - 最大值
*@return Number  - 中间值
*/
export function clamp(v, min, max) {
    var val = Math.max(v, min);
    return Math.min(val, max);
}
// parse xml
export function parseXml(xml) {
    if (window.DOMParser) {
        var parser = new DOMParser();
        return parser.parseFromString(xml, 'text/xml');
    }
    else {
        console.error('DOMParser is not supported in this browser.');
        return null;
    }
}
/*获取色彩插值算法渐变色.
 *@param String svg  - svg 序列化字符串
*@param String targetColor  - 目标色值#AABBCC 十六进制字符串
*@return String svg  - svg 序列化字符串
*@author lxl
*/
// export function setInterpolatedColor(svg: any, targetColor: string): Document | null {
//     var dom;
//     if (typeof (svg.documentElement) != 'undefined') {
//         // load from xml doc
//         dom = svg;
//     }
//     else {
//         // load from xml string
//         dom = parseXml(svg);
//     }
//     if (!dom)
//         return;
//     function rendColor(domEle, v) {
//         if (domEle.attributes['fill'] && !domEle.attributes['stop-color']) {
//             var clr = domEle.attributes['fill'].value;
//             if (clr != "none") {
//                 var rgb = parseColor(clr);
//                 var tgtclr = parseColor(v);
//                 if (rgb != null) {
//                     if (rgb.r == 0 && rgb.g == 0 && rgb.b == 0) {//黑色
//                         domEle.attributes['fill'].value = v;
//                     }
//                     // else if(rgb.r == 255 && rgb.g == 255 && rgb.b == 255){//白色
//                     //     domEle.attributes['fill'].value=v;
//                     // }
//                     else {
//                         if (rgb != null && tgtclr != null) {
//                             var endcolor = grayScaleToColorGradient(rgb, tgtclr);
//                             var interpolatedColor = getInterpolatedColor(rgb, endcolor, 1000);
//                             // var endclr = '#' + interpolatedColor.r.toString(16) + interpolatedColor.g.toString(16) + interpolatedColor.b.toString(16);
//                             var endclr = '#' + ((1 << 24) + (interpolatedColor.r << 16) + (interpolatedColor.g << 8) + interpolatedColor.b).toString(16).slice(1)
//                             domEle.attributes['fill'].value = endclr;
//                         }
//                     }
//                 }
//             }
//         }
//         if (domEle.attributes['stroke'] && !domEle.attributes['stop-color']) {
//             domEle.attributes['stroke'].value = v;
//         }
//         if (domEle.attributes['style']) {
//             if (domEle.attributes['style'].value.indexOf("fill") != -1 || domEle.attributes['style'].value.indexOf("stroke") != -1) {
//                 var data = domEle.attributes['style'].value.split(';');
//                 for (var i = 0; i < data.length; i++) {
//                     if (data[i].split(':')[0] === "fill") {
//                         var rgb = parseColor(data[i].split(':')[1]);
//                         if (rgb != null) {
//                             var tgtclr = parseColor(v);
//                             if (rgb.r == 0 && rgb.g == 0 && rgb.b == 0) {//黑色
//                                 data[i] = "fill:" + v;
//                             }
//                             //白色不变色
//                             // else if(rgb.r == 255 && rgb.g == 255 && rgb.b == 255){//白色
//                             //     data[i]= "fill:"+v;
//                             // }
//                             else {
//                                 if (rgb != null && tgtclr != null) {
//                                     //console.log(rgb)
//                                     var endcolor = grayScaleToColorGradient(rgb, tgtclr);
//                                     var interpolatedColor = getInterpolatedColor(rgb, endcolor, 1000);
//                                     var endclr = '#' + ((1 << 24) + (interpolatedColor.r << 16) + (interpolatedColor.g << 8) + interpolatedColor.b).toString(16).slice(1)
//                                     data[i] = "fill:" + endclr;
//                                 }
//                             }
//                         }
//                     } else if (data[i].split(':')[0] === "stroke") {
//                         //var rgb = Util.parseColor(data[i].split(':')[1]);
//                         //var tgtclr = Util.parseColor(v);
//                         // if(rgb != null && tgtclr != null){
//                         //     //console.log(rgb)
//                         //     var endcolor = NetColor.grayScaleToColorGradient(rgb, tgtclr);
//                         //     var interpolatedColor = NetColor.getInterpolatedColor(rgb, endcolor, 1000);
//                         //     var endclr = '#' + ((1 << 24) + (interpolatedColor.r << 16) + (interpolatedColor.g << 8) + interpolatedColor.b).toString(16).slice(1)
//                         //     data[i]= "stroke:"+endclr;
//                         // }
//                         data[i] = "stroke:" + v;
//                     }
//                 }
//                 domEle.attributes['style'].value = data.join(";");
//             }
//         }
//         if (domEle.attributes['stop-color']) {
//             var clr = domEle.attributes['stop-color'].value;
//             var rgb = parseColor(clr);
//             var tgtclr = parseColor(v);
//             if (rgb != null && tgtclr != null) {
//                 var endcolor = grayScaleToColorGradient(rgb, tgtclr);
//                 var interpolatedColor = getInterpolatedColor(rgb, endcolor, 1000);
//                 // var endclr = '#' + interpolatedColor.r.toString(16) + interpolatedColor.g.toString(16) + interpolatedColor.b.toString(16);
//                 var endclr = '#' + ((1 << 24) + (interpolatedColor.r << 16) + (interpolatedColor.g << 8) + interpolatedColor.b).toString(16).slice(1)
//                 domEle.attributes['stop-color'].value = endclr;
//             }
//         }
//         if (domEle.children.length > 0) {
//             for (var i = 0; i < domEle.children.length; i++) {
//                 rendColor(domEle.children[i], v);
//             }
//         }
//     }
//     rendColor(dom.documentElement, targetColor);
//     return dom;
// }
export function setInterpolatedColor(svg, targetColor) {
    var dom;
    if (typeof svg.documentElement !== 'undefined') {
        // load from xml doc
        dom = svg;
    }
    else {
        // load from xml string
        dom = parseXml(svg);
    }
    if (!dom) {
        return null;
    }
    function processStyle(styleString, v) {
        // 将url()的内容提取出来，然后在处理16进制颜色之前排除掉url()的部分
        var urlMatches = styleString.match(/(fill\s*:\s*url\([^\)]+\));/g);
        var removedUrls = [];
        if (urlMatches) {
            // 如果匹配到url()，将它们从styleString中去除
            urlMatches.forEach(function (urlMatch) {
                styleString = styleString.replace(urlMatch, '');
                removedUrls.push(urlMatch);
            });
        }
        // 现在styleString中不包含url()的内容，可以继续处理16进制颜色
        var data = styleString.split(';');
        for (var i = 0; i < data.length; i++) {
            var stylePair = data[i].split(':');
            if (stylePair.length >= 2) {
                var styleKey = stylePair[0].trim();
                var styleValue = stylePair[1].trim();
                if (styleKey === 'fill' || styleKey === 'stroke') {
                    if (styleValue != '') {
                        // 继续处理16进制颜色
                        var rgb = parseColor(styleValue);
                        if (rgb) {
                            var tgtclr = parseColor(v);
                            if (rgb.r === 0 && rgb.g === 0 && rgb.b === 0) {
                                data[i] = styleKey + ':' + v;
                            }
                            else if (rgb.r !== 255 || rgb.g !== 255 || rgb.b !== 255) {
                                if (tgtclr) {
                                    var endcolor = grayScaleToColorGradient(rgb, tgtclr);
                                    var interpolatedColor = getInterpolatedColor(rgb, endcolor, 1000);
                                    var endclr = '#' + ((1 << 24) + (interpolatedColor.r << 16) + (interpolatedColor.g << 8) + interpolatedColor.b).toString(16).slice(1);
                                    data[i] = styleKey + ':' + endclr;
                                }
                            }
                        }
                    }
                }
            }
        }
        // 将之前去除的url()内容重新加回到data中
        if (removedUrls.length > 0) {
            data = data.concat(removedUrls);
        }
        return data.join(';');
    }
    function processDefsStyle(defs, v) {
        var styleElements = defs.getElementsByTagName('style');
        for (var i = 0; i < styleElements.length; i++) {
            var styleElement = styleElements[i];
            var styleContent = styleElement.textContent;
            // 使用正则表达式匹配 { ... } 内的 CSS 内容
            var updatedStyle = styleContent.replace(/\{([^}]+)\}/g, function (match, cssContent) {
                // 在这里将提取到的 CSS 内容传递给 processStyle 处理
                var processedCss = processStyle(cssContent, v);
                return '{' + processedCss + '}';
            });
            styleElement.textContent = updatedStyle;
        }
    }
    function processElement(domEle, v) {
        if (domEle.attributes.fill && !domEle.attributes['stop-color']) {
            var clr = domEle.attributes.fill.value;
            if (clr !== 'none') {
                var rgb = parseColor(clr);
                var tgtclr = parseColor(v);
                if (rgb) {
                    if (rgb.r === 0 && rgb.g === 0 && rgb.b === 0) {
                        domEle.attributes.fill.value = v;
                    }
                    else if (rgb.r !== 255 || rgb.g !== 255 || rgb.b !== 255) {
                        if (tgtclr) {
                            var endcolor = grayScaleToColorGradient(rgb, tgtclr);
                            var interpolatedColor = getInterpolatedColor(rgb, endcolor, 1000);
                            var endclr = '#' + ((1 << 24) + (interpolatedColor.r << 16) + (interpolatedColor.g << 8) + interpolatedColor.b).toString(16).slice(1);
                            domEle.attributes.fill.value = endclr;
                        }
                    }
                }
            }
        }
        if (domEle.attributes.stroke && !domEle.attributes['stop-color']) {
            domEle.attributes.stroke.value = v;
        }
        if (domEle.attributes.style) {
            var styleValue = domEle.attributes.style.value;
            if (styleValue.indexOf('fill') !== -1 || styleValue.indexOf('stroke') !== -1) {
                var updatedStyle = processStyle(styleValue, v);
                domEle.attributes.style.value = updatedStyle;
            }
        }
        if (domEle.attributes['stop-color']) {
            var clr = domEle.attributes['stop-color'].value;
            var rgb = parseColor(clr);
            var tgtclr = parseColor(v);
            if (rgb && tgtclr) {
                var endcolor = grayScaleToColorGradient(rgb, tgtclr);
                var interpolatedColor = getInterpolatedColor(rgb, endcolor, 1000);
                var endclr = '#' + ((1 << 24) + (interpolatedColor.r << 16) + (interpolatedColor.g << 8) + interpolatedColor.b).toString(16).slice(1);
                domEle.attributes['stop-color'].value = endclr;
            }
        }
        if (domEle.children.length > 0) {
            for (var i = 0; i < domEle.children.length; i++) {
                processElement(domEle.children[i], v);
            }
        }
    }
    var defs = dom.getElementsByTagName('defs')[0];
    if (defs) {
        processDefsStyle(defs, targetColor);
    }
    processElement(dom.documentElement, targetColor);
    return dom;
}
/*
 * var endcolor = NetColor.grayScaleToColorGradient(sourceColor, targetColor);
 * var interpolatedColor = NetColor.getInterpolatedColor(sourceColor, endcolor, 1000); //目标色彩
 */
/*获取色彩插值算法渐变色.
 *@param {R,G,B} startColor  - 源色值RGB
*@param {R,G,B} endColor  - 目标色值RGB
*@param Number mills  - 梯度 0-1000
*@return {R,G,B} - 返回计算后的{R，G，B}值
*@author lxl
*/
function getInterpolatedColor(startColor, endColor, mills) {
    if (mills == 0) {
        return startColor;
    }
    if (mills == 1000) {
        return endColor;
    }
    mills = clamp(mills, 0, 1000);
    var red = Math.floor(startColor.r + mills * (endColor.r - startColor.r) / 1000);
    var green = Math.floor(startColor.g + mills * (endColor.g - startColor.g) / 1000);
    var blue = Math.floor(startColor.b + mills * (endColor.b - startColor.b) / 1000);
    return { r: red, g: green, b: blue };
}
/*调整色彩亮度
 *@param {R,G,B} sourceColor  - 源色值RGB
*@param Number adjust  - 梯度-1000~1000
*@return {R,G,B} - 返回计算后的{R，G，B}值
* @author lxl
*/
function getAdjustColorBrightness(sourceColor, adjust) {
    if (adjust > 0) {
        return getInterpolatedColor(sourceColor, { r: 255, g: 255, b: 255 }, adjust * 10);
    }
    if (adjust < 0) {
        return getInterpolatedColor(sourceColor, { r: 0, g: 0, b: 0 }, -adjust * 10);
    }
    return sourceColor;
}
/*灰度渐变
 * @param {R,G,B} sourceColor  - 源色值RGB
 * @param {R,G,B} targetColor  - 目标色值RGB
 * @return {R,G,B} - 返回计算后的{R，G，B}值
 * @author lxl
 */
function grayScaleToColorGradient(sourceColor, targetColor) {
    if (sourceColor.r == 0 && sourceColor.g == 0 && sourceColor.b == 0) { //黑色
        return { r: 0, g: 0, b: 0 };
    }
    if (sourceColor.r == 255 && sourceColor.g == 255 && sourceColor.b == 255) { //白色
        return { r: 255, g: 255, b: 255 };
    }
    var avgRGBValue = getAverageRGBValue(sourceColor);
    if (avgRGBValue < 128) {
        return getInterpolatedColor({ r: 0, g: 0, b: 0 }, targetColor, 1000 * avgRGBValue / 128);
    }
    return getInterpolatedColor(targetColor, { r: 255, g: 255, b: 255 }, 1000 * (avgRGBValue - 128) / 128);
}
/*计算色彩平均值
 * @param {R,G,B} color  - 源色值RGB
 * @return Number - 返回计算后的平均值
 * @author lxl
 */
function getAverageRGBValue(color) {
    return (color.r + color.g + color.b) / 3;
}
/*
 * Converts hex representation of RGB color (#33ffee) to object [r, g, b],
 * where r, g, b values are contained in the set [0, 255].
 *
 * @param {String} hex - Hex representation of rgb color
 * @return {Object} - in a form of
 *  {
 *  r - red color value,
 *  g - green color value,
 *  b - blue color value
 *  }
 *
 * Taken from: http://stackoverflow.com/a/5624139
 */
function hexToRgb(hex) {
    if (!hex) {
        return null; // 输入为空，返回 null
    }
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function (m, r, g, b) {
        return r + r + g + g + b + b;
    });
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result
        ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16),
        }
        : null;
}
function parseColor(color) {
    var hexRegex = /^#?([a-fA-F\d]{2})([a-fA-F\d]{2})([a-fA-F\d]{2})$/;
    var rgbaRegex = /^rgba\((\d+),\s*(\d+),\s*(\d+),\s*(\d+(\.\d+)?)\)$/;
    var result;
    if ((result = hexRegex.exec(color))) {
        // Input is a hex color string
        var r = parseInt(result[1], 16);
        var g = parseInt(result[2], 16);
        var b = parseInt(result[3], 16);
        return { r: r, g: g, b: b };
    }
    else if ((result = rgbaRegex.exec(color))) {
        // Input is an rgba color string
        var r = parseInt(result[1], 10);
        var g = parseInt(result[2], 10);
        var b = parseInt(result[3], 10);
        return { r: r, g: g, b: b };
    }
    return null;
}
//# sourceMappingURL=netColor.js.map