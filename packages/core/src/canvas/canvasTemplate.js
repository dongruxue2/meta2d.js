var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
import { ctxFlip, ctxRotate, drawImage, setGlobalAlpha, renderPen, } from '../pen';
import { createOffscreen } from './offscreen';
var CanvasTemplate = /** @class */ (function () {
    function CanvasTemplate(parentElement, store) {
        this.parentElement = parentElement;
        this.store = store;
        this.canvas = document.createElement('canvas');
        this.offscreen = createOffscreen();
        this.bgOffscreen = createOffscreen();
        parentElement.appendChild(this.canvas);
        this.canvas.style.backgroundRepeat = 'no-repeat';
        this.canvas.style.backgroundSize = '100% 100%';
        this.canvas.style.position = 'absolute';
        this.canvas.style.top = '0';
        this.canvas.style.left = '0';
    }
    CanvasTemplate.prototype.resize = function (w, h) {
        this.canvas.style.width = w + 'px';
        this.canvas.style.height = h + 'px';
        w = (w * this.store.dpiRatio) | 0;
        h = (h * this.store.dpiRatio) | 0;
        this.canvas.width = w;
        this.canvas.height = h;
        this.bgOffscreen.width = w;
        this.bgOffscreen.height = h;
        this.offscreen.width = w;
        this.offscreen.height = h;
        this.bgOffscreen
            .getContext('2d')
            .scale(this.store.dpiRatio, this.store.dpiRatio);
        this.bgOffscreen.getContext('2d').textBaseline = 'middle';
        this.offscreen
            .getContext('2d')
            .scale(this.store.dpiRatio, this.store.dpiRatio);
        this.offscreen.getContext('2d').textBaseline = 'middle';
        this.init();
    };
    CanvasTemplate.prototype.init = function () {
        this.bgOffscreen
            .getContext('2d')
            .clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.offscreen
            .getContext('2d')
            .clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.patchFlags = true;
        this.bgPatchFlags = true;
        // for (const pen of this.store.data.pens) {
        //   if (this.hasImage(pen)) {
        //     // 只影响本层的
        //     pen.calculative.imageDrawed = false;
        //   }
        // }
        // this.store.patchFlagsBackground = true;
        // this.store.patchFlagsTop = true;
    };
    CanvasTemplate.prototype.hidden = function () {
        this.canvas.style.display = 'none';
    };
    CanvasTemplate.prototype.show = function () {
        this.canvas.style.display = 'block';
    };
    CanvasTemplate.prototype.clear = function () {
        this.bgOffscreen
            .getContext('2d')
            .clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.offscreen
            .getContext('2d')
            .clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.canvas
            .getContext('2d')
            .clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.bgPatchFlags = true;
        this.patchFlags = true;
    };
    CanvasTemplate.prototype.render = function () {
        var e_1, _a;
        if (this.bgPatchFlags) {
            var ctx = this.bgOffscreen.getContext('2d');
            ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            var width = this.store.data.width || this.store.options.width;
            var height = this.store.data.height || this.store.options.height;
            var x = this.store.data.x || this.store.options.x;
            var y = this.store.data.y || this.store.options.y;
            var background = this.store.data.background || this.store.options.background;
            if (background) {
                ctx.save();
                ctx.fillStyle = background;
                if (width && height) {
                    ctx.fillRect(this.store.data.origin.x + x, this.store.data.origin.y + y, width * this.store.data.scale, height * this.store.data.scale);
                }
                else {
                    ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
                }
                ctx.restore();
            }
            if (width && height && this.store.bkImg) {
                ctx.save();
                ctx.drawImage(this.store.bkImg, this.store.data.origin.x + x, this.store.data.origin.y + y, width * this.store.data.scale, height * this.store.data.scale);
                ctx.restore();
            }
            this.renderGrid(ctx);
        }
        if (this.patchFlags) {
            var ctx = this.offscreen.getContext('2d');
            ctx.save();
            ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            ctx.translate(this.store.data.x, this.store.data.y);
            try {
                for (var _b = __values(this.store.data.pens), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var pen = _c.value;
                    if (!isFinite(pen.x)) {
                        continue;
                    }
                    if (pen.template && pen.calculative.inView) {
                        //非图片
                        renderPen(ctx, pen);
                        //图片
                        if (pen.image && pen.name !== 'gif' && pen.calculative.img) {
                            ctx.save();
                            ctxFlip(ctx, pen);
                            if (pen.calculative.rotate) {
                                ctxRotate(ctx, pen);
                            }
                            setGlobalAlpha(ctx, pen);
                            drawImage(ctx, pen);
                            ctx.restore();
                        }
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
            ctx.restore();
        }
        if (this.patchFlags || this.bgPatchFlags) {
            var ctxCanvas = this.canvas.getContext('2d');
            ctxCanvas.clearRect(0, 0, this.canvas.width, this.canvas.height);
            ctxCanvas.drawImage(this.bgOffscreen, 0, 0, this.canvas.width, this.canvas.height);
            ctxCanvas.drawImage(this.offscreen, 0, 0, this.canvas.width, this.canvas.height);
            this.patchFlags = false;
            this.bgPatchFlags = false;
        }
    };
    CanvasTemplate.prototype.renderGrid = function (ctx) {
        var _a = this.store, data = _a.data, options = _a.options;
        var grid = data.grid, gridRotate = data.gridRotate, gridColor = data.gridColor, gridSize = data.gridSize, scale = data.scale;
        if (!(grid !== null && grid !== void 0 ? grid : options.grid)) {
            // grid false 时不绘制, undefined 时看 options.grid
            return;
        }
        ctx.save();
        var _b = this.canvas, width = _b.width, height = _b.height;
        if (gridRotate) {
            ctx.translate(width / 2, height / 2);
            ctx.rotate((gridRotate * Math.PI) / 180);
            ctx.translate(-width / 2, -height / 2);
        }
        ctx.lineWidth = 1;
        ctx.strokeStyle = gridColor || options.gridColor;
        ctx.beginPath();
        var size = (gridSize || options.gridSize) * scale;
        var longSide = Math.max(width, height);
        var count = Math.ceil(longSide / size);
        for (var i = -size * count; i < longSide * 2; i += size) {
            ctx.moveTo(i, -longSide);
            ctx.lineTo(i, longSide * 2);
        }
        for (var i = -size * count; i < longSide * 2; i += size) {
            ctx.moveTo(-longSide, i);
            ctx.lineTo(longSide * 2, i);
        }
        ctx.stroke();
        ctx.restore();
    };
    return CanvasTemplate;
}());
export { CanvasTemplate };
//# sourceMappingURL=canvasTemplate.js.map