var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { getValidValue } from './common';
import { getFont } from "@meta2d/core";
//仪表全盘
export function gauge(ctx, pen) {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p;
    if (!pen.onAdd) {
        pen.onAdd = onAdd;
        pen.onDestroy = onDestroy;
        pen.onClick = onclick;
        if (pen.clockInterval) {
            pen.onDestroy(pen);
            pen.onAdd(pen);
        }
    }
    var x = pen.calculative.worldRect.x;
    var y = pen.calculative.worldRect.y;
    var w = pen.calculative.worldRect.width;
    var h = pen.calculative.worldRect.height;
    var scale = pen.calculative.canvas.store.data.scale;
    var basicConfigure = {
        startAngle: 225,
        endAngle: -45,
        min: 0,
        max: 100,
        splitNumber: 10,
    };
    //对echarts写法做兼容性处理
    if (pen.echarts && pen.echarts.option) {
        var series = pen.echarts.option.series[0];
        pen.startAngle = series.startAngle || basicConfigure.startAngle;
        pen.endAngle = series.endAngle || basicConfigure.endAngle;
        pen.min = series.min || basicConfigure.min;
        pen.max = series.max || basicConfigure.max;
        pen.axisLine = series.axisLine.lineStyle.color;
        pen.unit = series.detail.formatter.replace('{value}', '');
        pen.value = series.data[0].value;
        pen.splitNumber = series.splitNumber || basicConfigure.splitNumber;
    }
    pen = __assign(__assign({}, basicConfigure), pen);
    var r = w > h ? ((h / 2) * 9) / 10 : ((w / 2) * 9) / 10;
    var centerX = x + w / 2;
    var centerY = y + h / 2;
    var value = pen.echarts
        ? pen.echarts.option.series[0].data[0].value
        : pen.value;
    var pointColor;
    var gap = pen.startAngle - pen.endAngle;
    var bgColor = pen.background || '#E6EBF8';
    //绘制背景
    ctx.strokeStyle = bgColor;
    var bgLineWidth = r / 10;
    ctx.lineWidth = bgLineWidth;
    ctx.beginPath();
    ctx.lineCap = 'round';
    ctx.arc(centerX, centerY, r, (-pen.startAngle / 180) * Math.PI, (-pen.endAngle / 180) * Math.PI);
    ctx.stroke();
    ctx.closePath();
    var bili = 0;
    if (pen.axisLine && !pen.isClock) {
        for (var i = pen.axisLine.length - 1; i >= 0; i--) {
            if (pen.axisLine[i][0] * (pen.max - pen.min) < value) {
                bili = pen.axisLine[i][0];
            }
            else {
                bili = (value - pen.min) / (pen.max - pen.min);
                pointColor = pen.axisLine[i][1];
            }
            ctx.beginPath();
            ctx.strokeStyle = pen.axisLine[i][1];
            ctx.arc(centerX, centerY, r, (-pen.startAngle / 180) * Math.PI, ((-pen.startAngle + bili * gap) / 180) * Math.PI);
            ctx.stroke();
            ctx.closePath();
        }
    }
    ctx.lineCap = 'butt';
    //主刻度线
    var dashWidth = 2 * scale;
    var mainR = r - bgLineWidth;
    if (mainR < 0) {
        mainR = 0;
    }
    var arcLength = (gap / 180) * Math.PI * mainR;
    var dash = (arcLength - dashWidth * pen.splitNumber) / pen.splitNumber;
    var offsetAngle = ((gap / 180) * Math.PI * dashWidth) / 2 / arcLength;
    ctx.beginPath();
    ctx.strokeStyle = pen.color || '#999999';
    ctx.lineWidth = r / 20;
    ctx.setLineDash([dashWidth, dash]);
    ctx.arc(centerX, centerY, mainR, (-pen.startAngle / 180) * Math.PI - offsetAngle, (-pen.endAngle / 180) * Math.PI + offsetAngle);
    ctx.stroke();
    ctx.closePath();
    //从刻度线
    var fromDashWidth = 1 * scale;
    var fromR = r - bgLineWidth;
    if (fromR < 0) {
        fromR = 0;
    }
    var fromArcLength = (gap / 180) * Math.PI * fromR;
    var fromDash = (fromArcLength - fromDashWidth * 5 * pen.splitNumber) / 5 / pen.splitNumber;
    var fromOffsetAngle = ((gap / 180) * Math.PI * fromDashWidth) / 2 / fromArcLength;
    ctx.beginPath();
    ctx.strokeStyle = pen.color || '#999999';
    ctx.lineWidth = r / 40;
    ctx.setLineDash([fromDashWidth, fromDash]);
    ctx.arc(centerX, centerY, fromR, (-pen.startAngle / 180) * Math.PI - fromOffsetAngle, (-pen.endAngle / 180) * Math.PI + fromOffsetAngle);
    ctx.stroke();
    ctx.closePath();
    //绘制文字
    ctx.beginPath();
    var valueGap = pen.max - pen.min;
    var interval = valueGap / pen.splitNumber;
    var fontOption = {
        fontStyle: ((_a = pen.tickLabel) === null || _a === void 0 ? void 0 : _a.fontStyle) || pen.calculative.fontStyle,
        textDecoration: ((_b = pen.tickLabel) === null || _b === void 0 ? void 0 : _b.textDecoration) || pen.textDecoration,
        fontWeight: ((_c = pen.tickLabel) === null || _c === void 0 ? void 0 : _c.fontWeight) || pen.calculative.fontWeight,
        fontFamily: ((_d = pen.tickLabel) === null || _d === void 0 ? void 0 : _d.fontFamily) || pen.calculative.fontFamily,
        fontSize: (((_e = pen.tickLabel) === null || _e === void 0 ? void 0 : _e.fontSize) || pen.calculative.fontSize) * scale,
        lineHeight: ((_f = pen.tickLabel) === null || _f === void 0 ? void 0 : _f.lineHeight) || pen.calculative.lineHeight,
    };
    ctx.font = getFont(fontOption);
    var textR = r - bgLineWidth - r / 20;
    for (var i = 0; i <= pen.splitNumber; i++) {
        if (Math.abs(pen.startAngle) + Math.abs(pen.endAngle) === 360) {
            //形成一个圆形
            if (i == 0)
                continue;
        }
        var angle = pen.startAngle - ((interval * i) / valueGap) * gap;
        var width = Math.cos((angle / 180) * Math.PI);
        var height = Math.sin((angle / 180) * Math.PI);
        ctx.fillStyle = ((_g = pen.tickLabel) === null || _g === void 0 ? void 0 : _g.color) || '#999999';
        if (width > 0.02) {
            ctx.textAlign = 'end';
        }
        else if (width < -0.02) {
            ctx.textAlign = 'start';
        }
        else {
            ctx.textAlign = 'center';
        }
        if (height > 0.02) {
            ctx.textBaseline = 'top';
        }
        else if (height < -0.02) {
            ctx.textBaseline = 'bottom';
        }
        else {
            ctx.textBaseline = 'middle';
        }
        ctx.fillText(getValidValue(interval * i + pen.min, 1), centerX + textR * width, centerY - textR * height);
        ctx.fill();
    }
    ctx.closePath();
    //绘制指针
    var pointNum = 1;
    var valueArray = ['value'];
    if (pen.isClock) {
        pointNum = 3;
        valueArray = ['hourvalue', 'minutevalue', 'secondvalue'];
    }
    if (pen.isClock) {
        for (var i = 0; i < pointNum; i++) {
            var currentAngle = ((pen.startAngle -
                ((pen[valueArray[i]] - pen.min) / (pen.max - pen.min)) * gap) /
                180) *
                Math.PI;
            if (i > 0) {
                currentAngle =
                    ((pen.startAngle -
                        ((pen[valueArray[i]] - pen.min) / (pen.max * 5 - pen.min)) * gap) /
                        180) *
                        Math.PI;
            }
            var pointerR = (4 / 5) * r;
            if (valueArray[i] === 'hourvalue') {
                pointerR = (3 / 5) * r;
            }
            if (valueArray[i] === 'minutevalue') {
                pointerR = (3.5 / 5) * r;
            }
            var pointerHalfW = (r * 1) / 40;
            ctx.beginPath();
            ctx.setLineDash([]);
            ctx.lineWidth = r / (i + 1) / 20;
            ctx.strokeStyle = pen.color || '#999999';
            ctx.moveTo(centerX - pointerHalfW * 3 * Math.cos(currentAngle), centerY + pointerHalfW * 3 * Math.sin(currentAngle));
            ctx.lineTo(centerX + pointerR * Math.cos(currentAngle), centerY - pointerR * Math.sin(currentAngle));
            ctx.stroke();
        }
    }
    else {
        var currentAngle = ((pen.startAngle - ((value - pen.min) / (pen.max - pen.min)) * gap) /
            180) *
            Math.PI;
        var pointerR = (4 / 5) * r;
        var pointerHalfW = (r * 1) / 40;
        ctx.beginPath();
        ctx.setLineDash([]);
        ctx.lineWidth = 2;
        ctx.fillStyle = pointColor;
        ctx.moveTo(centerX - pointerHalfW * 3 * Math.cos(currentAngle), centerY + pointerHalfW * 3 * Math.sin(currentAngle));
        ctx.lineTo(centerX + pointerHalfW * Math.cos(currentAngle - Math.PI / 2), centerY - pointerHalfW * Math.sin(currentAngle - Math.PI / 2));
        ctx.lineTo(centerX + pointerR * Math.cos(currentAngle), centerY - pointerR * Math.sin(currentAngle));
        ctx.lineTo(centerX + pointerHalfW * Math.cos(currentAngle + Math.PI / 2), centerY - pointerHalfW * Math.sin(currentAngle + Math.PI / 2));
        ctx.lineTo(centerX - pointerHalfW * 3 * Math.cos(currentAngle), centerY + pointerHalfW * 3 * Math.sin(currentAngle));
        ctx.fill();
    }
    //文字描述
    ctx.beginPath();
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    var titleOption = {
        fontStyle: ((_h = pen.titleLabel) === null || _h === void 0 ? void 0 : _h.fontStyle) || pen.calculative.fontStyle,
        textDecoration: ((_j = pen.titleLabel) === null || _j === void 0 ? void 0 : _j.textDecoration) || pen.textDecoration,
        fontWeight: ((_k = pen.titleLabel) === null || _k === void 0 ? void 0 : _k.fontWeight) || pen.calculative.fontWeight,
        fontFamily: ((_l = pen.titleLabel) === null || _l === void 0 ? void 0 : _l.fontFamily) || pen.calculative.fontFamily,
        fontSize: (((_m = pen.titleLabel) === null || _m === void 0 ? void 0 : _m.fontSize) || pen.calculative.fontSize) * scale,
        lineHeight: ((_o = pen.titleLabel) === null || _o === void 0 ? void 0 : _o.lineHeight) || pen.calculative.lineHeight,
    };
    ctx.font = getFont(titleOption);
    ctx.fillStyle = ((_p = pen.titleLabel) === null || _p === void 0 ? void 0 : _p.color) || pointColor;
    if (pen.isClock) {
        ctx.fillText(('0' + parseInt(pen.hourvalue)).slice(-2) +
            ':' +
            ('0' + parseInt(pen.minutevalue)).slice(-2) +
            ':' +
            ('0' + parseInt(pen.secondvalue)).slice(-2), centerX, centerY + r / 2);
    }
    else {
        ctx.fillText(value + ' ' + (pen.unit || ''), centerX, centerY + r / 2);
    }
    ctx.fill();
    if (pen.isClock) {
        ctx.beginPath();
        ctx.fillStyle = pen.color || '#999999';
        ctx.strokeStyle = '#ffffff';
        ctx.arc(centerX, centerY, r / 20, 0, Math.PI * 2);
        ctx.stroke();
        ctx.fill();
        ctx.closePath();
    }
}
function onAdd(pen) {
    if (pen.isClock) {
        pen.clockInterval = setInterval(function () {
            var date = new Date();
            var second = date.getSeconds();
            var minute = date.getMinutes() + second / 60;
            var hour = (date.getHours() % 12) + minute / 60;
            pen.calculative.canvas.parent.setValue({
                id: pen.id,
                hourvalue: hour,
                minutevalue: minute,
                secondvalue: second,
            }, {
                render: true,
                doEvent: false,
            });
        }, 1000);
    }
    else {
        var tem_1 = pen.value;
        pen.value = 0;
        pen.frames = [
            {
                duration: 2000,
                value: tem_1,
            },
        ];
        pen.calculative.canvas.parent.startAnimate(pen.id);
        setTimeout(function () {
            pen.value = tem_1;
        }, 1000);
    }
}
function onDestroy(pen) {
    if (pen.clockInterval) {
        clearInterval(pen.clockInterval);
        pen.clockInterval = undefined;
    }
}
function onclick(pen) {
    if (pen.isClock) {
        pen.onDestroy(pen);
        pen.onAdd(pen);
    }
}
//# sourceMappingURL=gauge.js.map