/// <reference types="node" />
import { Pen } from '../../core/src/pen';
interface Axis {
    lineStyle?: {};
    axisLabel?: {
        fontSize?: number;
        fontColor?: string;
        fontStyle?: string;
        textDecoration?: string;
        fontWeight?: string;
        fontFamily?: string;
        lineHeight?: number;
    };
}
export declare function getValidValue(num: any, value: number): any;
export declare enum ReplaceMode {
    Add = 0,
    Replace = 1,
    ReplaceAll = 2
}
export interface leChartPen extends Pen {
    echarts?: {
        option: {
            xAxis: {
                data: any[];
            };
            series: any[];
            color: string[];
        };
    };
    tickLabel?: {
        show?: boolean;
        labelLine?: {
            show?: boolean;
        };
        color?: string;
        fontSize?: number;
        fontFamily?: string;
        fontWeight?: string;
        lineHeight?: number;
        fontStyle?: string;
        textDecoration?: string;
    };
    titleLabel?: {
        fontSize?: number;
        fontFamily?: string;
        fontWeight?: string;
        lineHeight?: number;
        fontStyle?: string;
        textDecoration?: string;
        color?: string;
    };
    xAxis?: Axis;
    yAxis?: Axis;
    startAngle?: number;
    endAngle?: number;
    min?: number;
    max?: number;
    axisLine?: [number, string];
    unit?: string;
    value?: number;
    splitNumber?: number;
    isClock?: boolean;
    hourvalue?: string;
    minutevalue?: string;
    secondvalue?: string;
    data?: any[];
    chartsColor?: string[];
    smooth?: boolean;
    chartsRadius?: string[][];
    frames?: leChartPen[];
    xAxisData?: string[];
    replaceMode?: ReplaceMode;
    clockInterval?: NodeJS.Timer;
}
export {};
