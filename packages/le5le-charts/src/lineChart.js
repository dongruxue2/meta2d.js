var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
import { coordinateAxis } from './coordinateAxis';
import { ReplaceMode } from './common';
//折线图
export function lineChart(ctx, pen) {
    if (!pen.onBeforeValue) {
        pen.onBeforeValue = beforeValue;
    }
    var x = pen.calculative.worldRect.x;
    var y = pen.calculative.worldRect.y;
    var w = pen.calculative.worldRect.width;
    var h = pen.calculative.worldRect.height;
    var scale = pen.calculative.canvas.store.data.scale;
    var series = [];
    if (pen.echarts && !pen.echarts.option.color) {
        pen.echarts.option.color = [
            '#1890ff',
            '#2FC25B',
            '#FACC14',
            '#c23531',
            '#2f4554',
            '#61a0a8',
            '#d48265',
        ];
    }
    var coordinate = coordinateAxis(ctx, pen);
    var dash = coordinate.dash;
    var normalizedOption = coordinate.normalizedOption;
    //数据值绘制
    var smooth = (pen.echarts ? pen.echarts.option.series[0].smooth : pen.smooth)
        ? true
        : false;
    var coordinateValue = [];
    if (pen.echarts) {
        for (var i = 0; i < pen.echarts.option.series.length; i++) {
            series.push(pen.echarts.option.series[i].data);
        }
    }
    else {
        series = pen.data;
    }
    var _loop_1 = function (j) {
        ctx.beginPath();
        var data = series[j];
        ctx.strokeStyle = pen.echarts
            ? pen.echarts.option.color[j]
            : pen.chartsColor[j];
        ctx.fillStyle = pen.echarts
            ? pen.echarts.option.color[j]
            : pen.chartsColor[j];
        var currentX = x + (1 + dash / 2);
        var currentY = y +
            h -
            ((data[0] - normalizedOption.min) /
                (normalizedOption.max - normalizedOption.min)) *
                h;
        ctx.moveTo(currentX, currentY);
        coordinateValue.push({ x: currentX, y: currentY });
        if (smooth) {
            //平滑的曲线
            if (data.length <= 2) {
                //小于两个点的情况无法构成贝塞尔
                for (var i = 1; i < data.length; i++) {
                    currentX = x + (1 + dash / 2) + (dash + 1) * i;
                    currentY =
                        y +
                            h -
                            ((data[i] - normalizedOption.min) /
                                (normalizedOption.max - normalizedOption.min)) *
                                h;
                    ctx.lineTo(currentX, currentY);
                    coordinateValue.push({ x: currentX, y: currentY });
                }
            }
            else {
                var cAx_1, cAy_1, cBx_1, cBy_1;
                data.forEach(function (item, index) {
                    currentX = x + (1 + dash / 2) + (dash + 1) * index;
                    currentY =
                        y +
                            h -
                            ((data[index] - normalizedOption.min) /
                                (normalizedOption.max - normalizedOption.min)) *
                                h;
                    var last1x = x + (1 + dash / 2) + (dash + 1) * (index + 1);
                    var last1y = y +
                        h -
                        ((data[index + 1] - normalizedOption.min) /
                            (normalizedOption.max - normalizedOption.min)) *
                            h;
                    var before1x = x + (1 + dash / 2) + (dash + 1) * (index - 1);
                    var before1y = y +
                        h -
                        ((data[index - 1] - normalizedOption.min) /
                            (normalizedOption.max - normalizedOption.min)) *
                            h;
                    var last2x = x + (1 + dash / 2) + (dash + 1) * (index + 2);
                    var last2y = y +
                        h -
                        ((data[index + 2] - normalizedOption.min) /
                            (normalizedOption.max - normalizedOption.min)) *
                            h;
                    if (index === 0) {
                        //第一个节点 用自己代替前一个节点
                        before1x = x + (1 + dash / 2) + (dash + 1) * index;
                        before1y =
                            y +
                                h -
                                ((data[index] - normalizedOption.min) /
                                    (normalizedOption.max - normalizedOption.min)) *
                                    h;
                    }
                    else if (index === data.length - 2) {
                        //倒数第二个节点 用下一个节点替代下下个节点
                        last2x = x + (1 + dash / 2) + (dash + 1) * (index + 1);
                        last2y =
                            y +
                                h -
                                ((data[index + 1] - normalizedOption.min) /
                                    (normalizedOption.max - normalizedOption.min)) *
                                    h;
                    }
                    coordinateValue.push({ x: currentX, y: currentY });
                    cAx_1 = currentX + (last1x - before1x) / 4;
                    cAy_1 = currentY + (last1y - before1y) / 4;
                    cBx_1 = last1x - (last2x - currentX) / 4;
                    cBy_1 = last1y - (last2y - currentY) / 4;
                    ctx.bezierCurveTo(cAx_1, cAy_1, cBx_1, cBy_1, last1x, last1y);
                    //绘制到下一个节点的贝塞尔曲线
                });
            }
        }
        else {
            for (var i = 1; i < data.length; i++) {
                currentX = x + (1 + dash / 2) + (dash + 1) * i;
                currentY =
                    y +
                        h -
                        ((data[i] - normalizedOption.min) /
                            (normalizedOption.max - normalizedOption.min)) *
                            h;
                ctx.lineTo(currentX, currentY);
                coordinateValue.push({ x: currentX, y: currentY });
            }
        }
        ctx.stroke();
        ctx.closePath();
        ctx.save();
        coordinateValue.forEach(function (item, index) {
            ctx.beginPath();
            ctx.strokeStyle = '#fff';
            ctx.lineWidth = 2 * scale;
            ctx.arc(item.x, item.y, 4 * scale, 0, Math.PI * 2);
            ctx.stroke();
            ctx.fill();
            ctx.closePath();
        });
        ctx.restore();
        coordinateValue = [];
    };
    for (var j = 0; j < series.length; j++) {
        _loop_1(j);
    }
}
export function beforeValue(pen, value) {
    if (value.xAxisData || value.data || (!value.dataX && !value.dataY)) {
        // 整体传参，不做处理
        return value;
    }
    var _xAxisData = pen.xAxisData;
    var _data = pen.data;
    var replaceMode = pen.replaceMode;
    var xAxisData = [];
    var data = [];
    if (!replaceMode) {
        //追加
        xAxisData = __spreadArray(__spreadArray([], __read(_xAxisData), false), __read(value.dataX), false);
        _data.forEach(function (item, index) {
            var _item = __spreadArray(__spreadArray([], __read(item), false), __read(value.dataY[index]), false);
            data.push(_item);
        });
    }
    else if (replaceMode === ReplaceMode.Replace) {
        //替换部分
        value.dataX.forEach(function (item, i) {
            var _index = _xAxisData.indexOf(item);
            _data.forEach(function (d, index) {
                d[_index] = value.dataY[index][i];
            });
        });
        xAxisData = _xAxisData;
        data = _data;
    }
    else if (replaceMode === ReplaceMode.ReplaceAll) {
        //全部替换
        xAxisData = value.dataX;
        data = value.dataY;
    }
    delete value.dataX;
    delete value.dataY;
    return Object.assign(value, { xAxisData: xAxisData, data: data });
}
//# sourceMappingURL=lineChart.js.map