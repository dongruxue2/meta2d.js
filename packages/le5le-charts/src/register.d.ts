import { lineChart } from './lineChart';
import { histogram } from './histogram';
import { pieChart } from './pieChart';
import { gauge } from './gauge';
export declare function chartsPens(): {
    lineChart: typeof lineChart;
    histogram: typeof histogram;
    pieChart: typeof pieChart;
    gauge: typeof gauge;
};
