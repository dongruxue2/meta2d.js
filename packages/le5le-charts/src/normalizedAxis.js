var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
// 双精度浮点数有效数字为15位
var maxDecimal = 15;
/**
 * 解决js的浮点数存在精度问题，在计算出最后结果时可以四舍五入一次，刻度太小也没有意义
 *
 * @export
 * @param {(number | string)} num
 * @param {number} [decimal=8]
 * @returns {number}
 */
export function fixedNum(num, decimal) {
    if (decimal === void 0) { decimal = maxDecimal; }
    var str = '' + num;
    if (str.indexOf('.') >= 0)
        str = Number.parseFloat(str).toFixed(decimal);
    return Number.parseFloat(str);
}
/**
 * 判断非Infinity非NaN的number
 *
 * @export
 * @param {*} num
 * @returns {num is number}
 */
export function numberValid(num) {
    return typeof num === 'number' && Number.isFinite(num);
}
/**
 * 计算理想的刻度值，刻度区间大小一般是[10, 15, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100]中某个数字的整10倍
 *
 * @export
 * @param {ScaleOption} option
 * @returns {ScaleResult}
 */
export function scaleCompute(option) {
    option = __assign({ max: null, min: null, splitNumber: 4, symmetrical: false, deviation: false, preferZero: false }, option);
    var magics = [
        10, 15, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100, 150,
    ]; // 加入150形成闭环
    // tslint:disable-next-line: prefer-const
    var dataMax = option.max, dataMin = option.min, splitNumber = option.splitNumber, symmetrical = option.symmetrical, deviation = option.deviation, preferZero = option.preferZero;
    if (!numberValid(dataMax) || !numberValid(dataMin) || dataMax < dataMin) {
        return { splitNumber: splitNumber };
    }
    else if (dataMax === dataMin && dataMax === 0) {
        return {
            max: fixedNum(magics[0] * splitNumber),
            min: dataMin,
            interval: magics[0],
            splitNumber: splitNumber,
        };
    }
    else if (dataMax === dataMin) {
        preferZero = true;
    }
    if (!numberValid(splitNumber) || splitNumber <= 0)
        splitNumber = 4;
    if (preferZero && dataMax * dataMin > 0) {
        if (dataMax < 0)
            dataMax = 0;
        else
            dataMin = 0;
    }
    var tempGap = (dataMax - dataMin) / splitNumber;
    var multiple = Math.floor(Math.log10(tempGap) - 1); // 指数
    multiple = Math.pow(10, multiple);
    var tempStep = tempGap / multiple;
    var expectedStep = magics[0] * multiple;
    var storedMagicsIndex = -1;
    var index; // 当前魔数下标
    for (index = 0; index < magics.length; index++) {
        if (magics[index] > tempStep) {
            expectedStep = magics[index] * multiple; // 取出第一个大于tempStep的魔数，并乘以multiple作为期望得到的最佳间隔
            break;
        }
    }
    var axisMax = dataMax;
    var axisMin = dataMin;
    function countDegree(step) {
        axisMax = parseInt('' + (dataMax / step + 1)) * step; // parseInt令小数去尾 -1.8 -> -1
        axisMin = parseInt('' + (dataMin / step - 1)) * step;
        if (dataMax === 0)
            axisMax = 0; // 优先0刻度
        if (dataMin === 0)
            axisMin = 0;
        if (symmetrical && axisMax * axisMin < 0) {
            var tm = Math.max(Math.abs(axisMax), Math.abs(axisMin));
            axisMax = tm;
            axisMin = -tm;
        }
    }
    countDegree(expectedStep);
    if (deviation) {
        return {
            max: fixedNum(axisMax),
            min: fixedNum(axisMin),
            interval: fixedNum(expectedStep),
            splitNumber: Math.round((axisMax - axisMin) / expectedStep),
        };
    }
    else if (!symmetrical || axisMax * axisMin > 0) {
        var tempSplitNumber = void 0;
        out: do {
            tempSplitNumber = Math.round((axisMax - axisMin) / expectedStep);
            if ((index - storedMagicsIndex) * (tempSplitNumber - splitNumber) < 0) {
                // 出现死循环
                while (tempSplitNumber < splitNumber) {
                    if ((axisMin - dataMin <= axisMax - dataMax && axisMin !== 0) ||
                        axisMax === 0) {
                        axisMin -= expectedStep;
                    }
                    else {
                        axisMax += expectedStep;
                    }
                    tempSplitNumber++;
                    if (tempSplitNumber === splitNumber)
                        break out;
                }
            }
            if (index >= magics.length - 1 ||
                index <= 0 ||
                tempSplitNumber === splitNumber)
                break;
            storedMagicsIndex = index;
            if (tempSplitNumber > splitNumber)
                expectedStep = magics[++index] * multiple;
            else
                expectedStep = magics[--index] * multiple;
            countDegree(expectedStep);
        } while (tempSplitNumber !== splitNumber);
    }
    axisMax = fixedNum(axisMax);
    axisMin = fixedNum(axisMin);
    var interval = fixedNum((axisMax - axisMin) / splitNumber);
    return {
        max: axisMax,
        min: axisMin,
        interval: interval,
        splitNumber: splitNumber,
    };
}
//# sourceMappingURL=normalizedAxis.js.map