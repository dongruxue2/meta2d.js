import { leChartPen } from './common';
export declare function lineChart(ctx: CanvasRenderingContext2D, pen: leChartPen): void;
export declare function beforeValue(pen: leChartPen, value: any): any;
