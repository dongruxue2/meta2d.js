import { coordinateAxis } from './coordinateAxis';
import { beforeValue } from './lineChart';
//柱状图
export function histogram(ctx, pen) {
    if (!pen.onBeforeValue) {
        pen.onBeforeValue = beforeValue;
    }
    var scale = pen.calculative.canvas.store.data.scale;
    var x = pen.calculative.worldRect.x;
    var y = pen.calculative.worldRect.y;
    var w = pen.calculative.worldRect.width;
    var h = pen.calculative.worldRect.height;
    var series = [];
    if (pen.echarts && !pen.echarts.option.color) {
        pen.echarts.option.color = [
            '#1890ff',
            '#2FC25B',
            '#FACC14',
            '#c23531',
            '#2f4554',
            '#61a0a8',
            '#d48265',
        ];
    }
    if (pen.echarts) {
        for (var i = 0; i < pen.echarts.option.series.length; i++) {
            series.push(pen.echarts.option.series[i].data);
        }
    }
    else {
        series = pen.data;
    }
    var coordinate = coordinateAxis(ctx, pen);
    var dash = coordinate.dash;
    var normalizedOption = coordinate.normalizedOption;
    var itemWidth = (dash * 4) / 5 / series.length;
    // ctx.strokeStyle = '#ffffff';
    for (var j = 0; j < series.length; j++) {
        ctx.beginPath();
        var data = series[j];
        ctx.fillStyle = pen.echarts
            ? pen.echarts.option.color[j]
            : pen.chartsColor[j];
        ctx.strokeStyle = '#ffffff';
        ctx.lineWidth = 1 * scale;
        var currentX = 0;
        var currentY = 0;
        var currentH = 0;
        for (var i = 0; i < data.length; i++) {
            currentX = x + (1 + 0.1 * dash) + (dash + 1) * i + itemWidth * j;
            currentH =
                ((data[i] - normalizedOption.min) /
                    (normalizedOption.max - normalizedOption.min)) *
                    h;
            currentY = y + h - currentH;
            //宽度-1是为了数据之间的间距 高度-1是为了坐标轴不被覆盖
            ctx.rect(currentX, currentY, itemWidth - 1, currentH - 1);
            ctx.stroke();
            ctx.fill();
        }
        ctx.closePath();
    }
    // let;
}
//# sourceMappingURL=histogram.js.map