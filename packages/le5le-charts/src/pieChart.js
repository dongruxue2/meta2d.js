var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
import { ReplaceMode } from './common';
import { getFont } from "@meta2d/core";
//饼状图
export function pieChart(ctx, pen) {
    var _a, _b;
    if (!pen.onBeforeValue) {
        pen.onBeforeValue = beforeValue;
    }
    // 缩放模式
    var scale = pen.calculative.canvas.store.data.scale;
    var x = pen.calculative.worldRect.x;
    var y = pen.calculative.worldRect.y;
    var w = pen.calculative.worldRect.width;
    var h = pen.calculative.worldRect.height;
    var isEcharts = pen.echarts ? true : false;
    // if (pen.echarts && !pen.echarts.option.color) {
    //   pen.echarts.option.color = [
    //     '#1890ff',
    //     '#2FC25B',
    //     '#FACC14',
    //     '#c23531',
    //     '#2f4554',
    //     '#61a0a8',
    //     '#d48265',
    //   ];
    // } else {
    //   pen.chartsColor = [
    //     '#1890ff',
    //     '#2FC25B',
    //     '#FACC14',
    //     '#c23531',
    //     '#2f4554',
    //     '#61a0a8',
    //     '#d48265',
    //   ];
    // }
    if (pen.echarts) {
        if (!pen.echarts.option.color) {
            pen.echarts.option.color = [
                '#1890ff',
                '#2FC25B',
                '#FACC14',
                '#c23531',
                '#2f4554',
                '#61a0a8',
                '#d48265',
            ];
        }
        pen.chartsColor = pen.echarts.option.color;
    }
    else {
        if (!pen.chartsColor) {
            pen.chartsColor = [
                '#1890ff',
                '#2FC25B',
                '#FACC14',
                '#c23531',
                '#2f4554',
                '#61a0a8',
                '#d48265',
            ];
        }
    }
    var seriesArray = isEcharts ? pen.echarts.option.series : pen.data;
    var beforeSeriesLength = 0;
    var _loop_1 = function (ser) {
        var series = seriesArray[ser];
        var r = w / 2;
        if (h < w) {
            r = h / 2;
        }
        var centerX = x + w / 2;
        var centerY = y + h / 2;
        var sum = 0;
        if (isEcharts) {
            sum = series.data.reduce(function (prev, curr) {
                return prev + curr.value;
            }, 0);
        }
        else {
            sum = series.reduce(function (prev, curr) {
                return prev + curr.value;
            }, 0);
        }
        var fromR = (r *
            parseFloat(isEcharts ? series.radius[0] : pen.chartsRadius[ser][0])) /
            100;
        var toR = (r *
            parseFloat(isEcharts ? series.radius[1] : pen.chartsRadius[ser][1])) /
            100;
        if (fromR > toR) {
            return { value: void 0 };
        }
        var beforeAngle = 0;
        var afterAngle = 0;
        ctx.strokeStyle = isEcharts
            ? ((_a = series.itemStyle) === null || _a === void 0 ? void 0 : _a.borderColor) || '#fff'
            : '#fff';
        ctx.lineWidth = (isEcharts ? ((_b = series.itemStyle) === null || _b === void 0 ? void 0 : _b.borderWidth) || 2 : 2) * scale;
        var data = isEcharts ? series.data : series;
        data.forEach(function (item, index) {
            var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o;
            afterAngle += (Math.PI * 2 * item.value) / sum;
            ctx.beginPath();
            var colorLength = beforeSeriesLength + index;
            if (colorLength >= pen.chartsColor.length) {
                colorLength = colorLength % pen.chartsColor.length;
            }
            ctx.fillStyle = isEcharts
                ? pen.echarts.option.color[colorLength]
                : pen.chartsColor[colorLength];
            ctx.moveTo(centerX + fromR * Math.sin(afterAngle), centerY - fromR * Math.cos(afterAngle));
            ctx.arc(centerX, centerY, fromR, -Math.PI / 2 + afterAngle, -Math.PI / 2 + beforeAngle, true);
            ctx.lineTo(centerX + toR * Math.sin(beforeAngle), centerY - toR * Math.cos(beforeAngle));
            ctx.arc(centerX, centerY, toR, -Math.PI / 2 + beforeAngle, -Math.PI / 2 + afterAngle);
            ctx.lineTo(centerX + fromR * Math.sin(afterAngle), centerY - fromR * Math.cos(afterAngle));
            ctx.stroke();
            ctx.fill();
            ctx.closePath();
            //绘制label
            var centerAngle = (beforeAngle + afterAngle) / 2;
            var temX = centerX + (toR + 10 * scale) * Math.sin(centerAngle);
            var temY = centerY - (toR + 10 * scale) * Math.cos(centerAngle);
            var temFillStyle = ctx.fillStyle;
            if (!series.label) {
                series.label = { position: 'outside', show: true };
            }
            if (isEcharts && ['inner', 'inside'].includes(series.label.position)) {
                ctx.fillStyle = '#ffffff';
                temX = centerX + ((toR - fromR) / 2) * Math.sin(centerAngle);
                temY = centerY - ((toR - fromR) / 2) * Math.cos(centerAngle);
            }
            else if (isEcharts && series.label.position == 'outside') {
            }
            if (!series.labelLine) {
                series.labelLine = { show: true };
            }
            if ((isEcharts && series.labelLine.show !== false) || !isEcharts) {
                ctx.beginPath();
                ctx.strokeStyle = isEcharts
                    ? pen.echarts.option.color[beforeSeriesLength + index]
                    : pen.chartsColor[beforeSeriesLength + index];
                ctx.moveTo(centerX + toR * Math.sin(centerAngle), centerY - toR * Math.cos(centerAngle));
                ctx.lineTo(temX, temY);
            }
            var fontOption = {
                fontStyle: ((_a = pen.tickLabel) === null || _a === void 0 ? void 0 : _a.fontStyle) || pen.calculative.fontStyle,
                fontWeight: ((_b = pen.tickLabel) === null || _b === void 0 ? void 0 : _b.fontWeight) || pen.calculative.fontWeight,
                fontFamily: ((_c = pen.tickLabel) === null || _c === void 0 ? void 0 : _c.fontFamily) || pen.calculative.fontFamily,
                lineHeight: ((_d = pen.tickLabel) === null || _d === void 0 ? void 0 : _d.lineHeight) || pen.calculative.lineHeight,
                fontSize: (((_e = pen.tickLabel) === null || _e === void 0 ? void 0 : _e.fontSize) || pen.calculative.fontSize) * scale
            };
            ctx.font = getFont(fontOption); // r / 10 + 'px AlibabaPuHuiTi-Regular, Alibaba PuHuiTi';
            ctx.textBaseline = 'middle';
            ctx.textAlign = 'center';
            // 写入文字
            if (centerAngle > Math.PI) {
                if ((isEcharts && series.label.position === 'outside') || !isEcharts) {
                    ctx.textAlign = 'end';
                }
                if ((isEcharts && series.labelLine.show !== false) || (!isEcharts && ((_h = (_g = (_f = pen.tickLabel) === null || _f === void 0 ? void 0 : _f.labelLine) === null || _g === void 0 ? void 0 : _g.show) !== null && _h !== void 0 ? _h : true))) {
                    ctx.lineTo(temX - 5 * scale, temY);
                }
                if ((isEcharts && series.label.show !== false) || (!isEcharts && ((_k = (_j = pen.tickLabel) === null || _j === void 0 ? void 0 : _j.show) !== null && _k !== void 0 ? _k : true))) {
                    ctx.fillText(item.name, temX - 5 * scale, temY);
                }
            }
            else {
                if ((isEcharts && series.label.position === 'outside') || !isEcharts) {
                    ctx.textAlign = 'start';
                }
                if ((isEcharts && series.labelLine.show !== false) || !isEcharts) {
                    ctx.lineTo(temX + 5 * scale, temY);
                }
                if ((isEcharts && series.label.show !== false) || (!isEcharts && ((_m = (_l = pen.tickLabel) === null || _l === void 0 ? void 0 : _l.show) !== null && _m !== void 0 ? _m : true))) {
                    ctx.fillText(item.name, temX + 5 * scale, temY);
                }
            }
            ctx.stroke();
            ctx.closePath();
            ctx.fillStyle = temFillStyle;
            ctx.strokeStyle = isEcharts
                ? ((_o = series.itemStyle) === null || _o === void 0 ? void 0 : _o.borderColor) || '#fff'
                : '#fff';
            beforeAngle = afterAngle;
        });
        beforeSeriesLength += data.length;
    };
    for (var ser = 0; ser < seriesArray.length; ser++) {
        var state_1 = _loop_1(ser);
        if (typeof state_1 === "object")
            return state_1.value;
    }
}
function beforeValue(pen, value) {
    if (value.data || (!value.dataX && !value.dataY)) {
        // 整体传参，不做处理
        return value;
    }
    var _data = pen.data;
    var replaceMode = pen.replaceMode;
    var data = [];
    if (!replaceMode) {
        //追加
        _data.forEach(function (item, index) {
            var _item = __spreadArray(__spreadArray([], __read(item), false), __read(value.dataY[index]), false);
            data.push(_item);
        });
    }
    else if (replaceMode === ReplaceMode.Replace) {
        //替换部分
        value.dataY.forEach(function (item, index) {
            item.forEach(function (_innerItem, _innderIndex) {
                var _filterItem = _data[index].filter(function (_i) { return _i.name === _innerItem.name; });
                if (_filterItem.length > 0) {
                    _filterItem[0].value = _innerItem.value;
                }
            });
        });
        data = _data;
    }
    else if (replaceMode === ReplaceMode.ReplaceAll) {
        //全部替换
        data = value.dataY;
    }
    delete value.dataX;
    delete value.dataY;
    return Object.assign(value, { data: data });
}
//# sourceMappingURL=pieChart.js.map