export function getValidValue(num, value) {
    if (isNaN(num)) {
        return;
    }
    if (value === -1) {
        return num;
    }
    // return num - parseInt(num) == 0 ? num : Number(num).toFixed(value);
    // return Number(num).toFixed(value);
    return Math.round(Number(num) * 1000) / 1000;
}
export var ReplaceMode;
(function (ReplaceMode) {
    ReplaceMode[ReplaceMode["Add"] = 0] = "Add";
    ReplaceMode[ReplaceMode["Replace"] = 1] = "Replace";
    ReplaceMode[ReplaceMode["ReplaceAll"] = 2] = "ReplaceAll";
})(ReplaceMode || (ReplaceMode = {}));
//# sourceMappingURL=common.js.map