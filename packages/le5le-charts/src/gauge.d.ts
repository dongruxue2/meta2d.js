import { leChartPen } from './common';
export declare function gauge(ctx: CanvasRenderingContext2D, pen: leChartPen): void;
