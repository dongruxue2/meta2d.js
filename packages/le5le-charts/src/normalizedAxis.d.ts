export interface ScaleOption {
    /**
     * 数据最大值
     *
     * @type {(number | null)}
     * @memberof ScaleOption
     */
    max: number | null;
    /**
     * 数据最小值
     *
     * @type {(number | null)}
     * @memberof ScaleOption
     */
    min: number | null;
    /**
     * 预期分成几个区间
     *
     * @type {number}
     * @memberof ScaleOption
     */
    splitNumber?: number;
    /**
     * 存在异号数据时正负区间是否需要对称
     *
     * @type {boolean}
     * @memberof ScaleOption
     */
    symmetrical?: boolean;
    /**
     * 是否允许实际分成的区间数有误差
     *
     * @type {boolean}
     * @memberof ScaleOption
     */
    deviation?: boolean;
    /**
     * 是否优先取到0刻度
     *
     * @type {boolean}
     * @memberof ScaleOption
     */
    preferZero?: boolean;
}
export interface ScaleResult {
    max?: number;
    min?: number;
    interval?: number;
    splitNumber?: number;
}
/**
 * 解决js的浮点数存在精度问题，在计算出最后结果时可以四舍五入一次，刻度太小也没有意义
 *
 * @export
 * @param {(number | string)} num
 * @param {number} [decimal=8]
 * @returns {number}
 */
export declare function fixedNum(num: number | string, decimal?: number): number;
/**
 * 判断非Infinity非NaN的number
 *
 * @export
 * @param {*} num
 * @returns {num is number}
 */
export declare function numberValid(num: any): num is number;
/**
 * 计算理想的刻度值，刻度区间大小一般是[10, 15, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100]中某个数字的整10倍
 *
 * @export
 * @param {ScaleOption} option
 * @returns {ScaleResult}
 */
export declare function scaleCompute(option: ScaleOption): ScaleResult;
