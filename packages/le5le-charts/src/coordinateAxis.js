import { scaleCompute } from './normalizedAxis';
import { getFont } from "@meta2d/core";
//用于绘制坐标轴
export function coordinateAxis(ctx, pen) {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u, _v, _w, _x, _y, _z, _0, _1, _2, _3;
    var x = pen.calculative.worldRect.x;
    var y = pen.calculative.worldRect.y;
    var w = pen.calculative.worldRect.width;
    var h = pen.calculative.worldRect.height;
    // 缩放比例
    var r = w / 2;
    var scale = pen.calculative.canvas.store.data.scale;
    var series = [];
    if (pen.echarts) {
        for (var i = 0; i < pen.echarts.option.series.length; i++) {
            series.push(pen.echarts.option.series[i].data);
        }
    }
    else {
        series = pen.data;
    }
    var collection_data = [];
    for (var i = 0; i < series.length; i++) {
        collection_data = collection_data.concat(series[i]);
    }
    var initOption = {
        max: Math.max.apply(null, collection_data),
        min: Math.min.apply(null, collection_data),
        splitNumber: 5,
    };
    var normalizedOption = scaleCompute(initOption);
    var num = pen.echarts
        ? pen.echarts.option.xAxis.data.length
        : pen.xAxisData.length;
    ctx.beginPath();
    ctx.strokeStyle = '#BFBFBF';
    ctx.lineWidth = 6 * scale;
    ctx.lineCap = 'butt';
    var dash = (w - 1 * (num + 1)) / num;
    ctx.setLineDash([1, dash]);
    ctx.moveTo(x, y + h + 3 * scale);
    ctx.lineTo(x + w, y + h + 3 * scale);
    ctx.stroke();
    ctx.closePath();
    //x轴绘制
    ctx.beginPath();
    ctx.lineWidth = 1 * scale;
    ctx.setLineDash([]);
    ctx.moveTo(x, y + h);
    ctx.lineTo(x + w, y + h);
    ctx.stroke();
    ctx.closePath();
    //y轴绘制
    ctx.beginPath();
    ctx.fillStyle = '#BFBFBF';
    ctx.strokeStyle = '#E9E9E9';
    ctx.setLineDash([2, 2]);
    var fontOptions = {
        fontStyle: ((_b = (_a = pen.yAxis) === null || _a === void 0 ? void 0 : _a.axisLabel) === null || _b === void 0 ? void 0 : _b.fontStyle) || pen.fontStyle,
        textDecoration: (_d = (_c = pen.yAxis) === null || _c === void 0 ? void 0 : _c.axisLabel) === null || _d === void 0 ? void 0 : _d.textDecoration,
        fontWeight: ((_f = (_e = pen.yAxis) === null || _e === void 0 ? void 0 : _e.axisLabel) === null || _f === void 0 ? void 0 : _f.fontWeight) || pen.fontWeight,
        fontFamily: ((_h = (_g = pen.yAxis) === null || _g === void 0 ? void 0 : _g.axisLabel) === null || _h === void 0 ? void 0 : _h.fontFamily) || pen.fontFamily,
        fontSize: ((_k = (_j = pen.yAxis) === null || _j === void 0 ? void 0 : _j.axisLabel) === null || _k === void 0 ? void 0 : _k.fontSize) || pen.fontSize,
        lineHeight: ((_m = (_l = pen.yAxis) === null || _l === void 0 ? void 0 : _l.axisLabel) === null || _m === void 0 ? void 0 : _m.lineHeight) || pen.lineHeight,
    };
    ctx.fillStyle = ((_p = (_o = pen.yAxis) === null || _o === void 0 ? void 0 : _o.axisLabel) === null || _p === void 0 ? void 0 : _p.fontColor) || pen.color;
    for (var i = 0; i < normalizedOption.splitNumber + 1; i++) {
        var temH = (i * h) / normalizedOption.splitNumber;
        ctx.textAlign = 'right';
        ctx.textBaseline = 'middle';
        ctx.font = getFont(fontOptions); //r / 10 +'px AlibabaPuHuiTi-Regular, Alibaba PuHuiTi';
        ctx.fillText(normalizedOption.max - i * normalizedOption.interval + '', x - 10 * scale, y + temH);
        ctx.fill();
        if (i < normalizedOption.splitNumber) {
            ctx.beginPath();
            ctx.moveTo(x, y + temH);
            ctx.lineTo(x + w, y + temH);
            ctx.stroke();
        }
    }
    ctx.closePath();
    //x轴下标绘制
    ctx.beginPath();
    ctx.strokeStyle = '#BFBFBF';
    var xData = pen.echarts ? pen.echarts.option.xAxis.data : pen.xAxisData;
    var xdataX = 0;
    for (var i = 0; i < xData.length; i++) {
        xdataX = x + (1 + dash / 2) + (dash + 1) * i;
        ctx.textAlign = 'center';
        ctx.textBaseline = 'top';
        var fontOptions_1 = {
            fontStyle: ((_r = (_q = pen.xAxis) === null || _q === void 0 ? void 0 : _q.axisLabel) === null || _r === void 0 ? void 0 : _r.fontStyle) || pen.calculative.fontStyle,
            textDecoration: (_t = (_s = pen.xAxis) === null || _s === void 0 ? void 0 : _s.axisLabel) === null || _t === void 0 ? void 0 : _t.textDecoration,
            fontWeight: ((_v = (_u = pen.xAxis) === null || _u === void 0 ? void 0 : _u.axisLabel) === null || _v === void 0 ? void 0 : _v.fontWeight) || pen.calculative.fontWeight,
            fontFamily: ((_x = (_w = pen.xAxis) === null || _w === void 0 ? void 0 : _w.axisLabel) === null || _x === void 0 ? void 0 : _x.fontFamily) || pen.calculative.fontFamily,
            fontSize: ((_z = (_y = pen.xAxis) === null || _y === void 0 ? void 0 : _y.axisLabel) === null || _z === void 0 ? void 0 : _z.fontSize) || pen.calculative.fontSize,
            lineHeight: ((_1 = (_0 = pen.xAxis) === null || _0 === void 0 ? void 0 : _0.axisLabel) === null || _1 === void 0 ? void 0 : _1.lineHeight) || pen.calculative.lineHeight,
        };
        ctx.font = getFont(fontOptions_1); //r / 10 +'px AlibabaPuHuiTi-Regular, Alibaba PuHuiTi';
        ctx.fillStyle = ((_3 = (_2 = pen.xAxis) === null || _2 === void 0 ? void 0 : _2.axisLabel) === null || _3 === void 0 ? void 0 : _3.fontColor) || pen.calculative.color;
        ctx.fillText(xData[i], xdataX, (y + h + 10 * scale));
        ctx.fill();
    }
    ctx.closePath();
    ctx.setLineDash([]);
    return { dash: dash, normalizedOption: normalizedOption };
}
//# sourceMappingURL=coordinateAxis.js.map