import { leChartPen } from './common';
export declare function histogram(ctx: CanvasRenderingContext2D, pen: leChartPen): void;
