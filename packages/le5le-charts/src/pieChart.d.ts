import { leChartPen } from './common';
export declare function pieChart(ctx: CanvasRenderingContext2D, pen: leChartPen): void;
