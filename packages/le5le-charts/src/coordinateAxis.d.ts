import { leChartPen } from './common';
export declare function coordinateAxis(ctx: CanvasRenderingContext2D, pen: leChartPen): {
    dash: number;
    normalizedOption: import("./normalizedAxis").ScaleResult;
};
