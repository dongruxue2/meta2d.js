export declare function register(_echarts?: any): void;
export declare function registerHighcharts(_highcharts?: any): void;
export declare function registerLightningChart(_lightningCharts?: any): void;
