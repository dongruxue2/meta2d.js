import { register as meta2dRegister } from '@meta2d/core';
import { echarts } from './echarts';
import { highcharts } from './highcharts';
import { lightningCharts } from './LightningChart';
export function register(_echarts) {
    _echarts && (globalThis.echarts = _echarts);
    meta2dRegister({ echarts: echarts });
}
export function registerHighcharts(_highcharts) {
    _highcharts && (globalThis.Highcharts = _highcharts);
    meta2dRegister({ highcharts: highcharts });
}
export function registerLightningChart(_lightningCharts) {
    _lightningCharts && (globalThis.lcjs = _lightningCharts);
    meta2dRegister({ lightningCharts: lightningCharts });
}
//# sourceMappingURL=register.js.map