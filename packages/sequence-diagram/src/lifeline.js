export function lifeline(ctx, pen) {
    var _a;
    var headHeight = (_a = pen.headHeight) !== null && _a !== void 0 ? _a : 50;
    var _b = pen.calculative.worldRect, x = _b.x, y = _b.y, width = _b.width, height = _b.height, ey = _b.ey;
    var wr = pen.calculative.borderRadius || 0, hr = wr;
    if (pen.calculative.borderRadius < 1) {
        wr *= width;
        hr *= height;
    }
    var r = wr < hr ? wr : hr;
    if (width < 2 * r) {
        r = width / 2;
    }
    if (headHeight < 2 * r) {
        r = headHeight / 2;
    }
    ctx.beginPath();
    ctx.moveTo(x + r, y);
    ctx.arcTo(x + width, y, x + width, y + headHeight, r);
    ctx.arcTo(x + width, y + headHeight, x, y + headHeight, r);
    ctx.arcTo(x, y + headHeight, x, y, r);
    ctx.arcTo(x, y, x + width, y, r);
    ctx.closePath();
    ctx.stroke();
    ctx.save();
    ctx.beginPath();
    ctx.lineWidth = 1;
    ctx.setLineDash([7, 7]);
    var middle = x + width / 2;
    ctx.moveTo(middle, y + headHeight + 1);
    ctx.lineTo(middle, ey);
    ctx.stroke();
    ctx.restore();
}
//# sourceMappingURL=lifeline.js.map