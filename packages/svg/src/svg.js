var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
import { getRectOfPoints, s8 } from '@meta2d/core';
import { getRect, parseSvgPath } from '@meta2d/core/src/diagrams/svg/parse';
import { XMLParser } from 'fast-xml-parser/src/fxp';
var selfName = ':@';
var allRect;
var shapeScale; // 图形缩小比例
var anchorsArr = [];
// globalThis.parseSvg = parseSvg; //  测试
export function parseSvg(svg) {
    var parser = new XMLParser({
        ignoreAttributes: false,
        attributeNamePrefix: '',
        preserveOrder: true,
    });
    var xmlJson = parser.parse(svg);
    var svgs = xmlJson.filter(function (item) { return item.svg; });
    var pens = [];
    anchorsArr = [];
    svgs.forEach(function (svg) {
        var selfProperty = svg[selfName];
        allRect = transformContainerRect(selfProperty);
        var isWidthLitter = allRect.width < allRect.height; // 宽小于高
        // 长边等于 40
        if (!isWidthLitter) {
            if (!selfProperty.width) {
                selfProperty.width = 40;
            }
            shapeScale = selfProperty.width / allRect.width;
            !selfProperty.height &&
                (selfProperty.height = shapeScale * allRect.height);
        }
        else {
            // 高小于宽
            if (!selfProperty.height) {
                selfProperty.height = 40;
            }
            shapeScale = selfProperty.height / allRect.height;
            !selfProperty.width && (selfProperty.width = shapeScale * allRect.width);
        }
        var children = svg.svg;
        var combinePens = transformCombines(selfProperty, children);
        pens.push.apply(pens, __spreadArray([], __read(combinePens), false));
    });
    setAnchor(pens[0]);
    return pens;
}
function setAnchor(pen) {
    anchorsArr = anchorsArr.map(function (item, index) {
        return {
            id: index + '',
            penId: pen.id,
            x: item.x,
            y: item.y,
        };
    });
    pen.anchors = anchorsArr;
}
function transformCombines(selfProperty, children) {
    var pens = [];
    var _a = __read(dealUnit(selfProperty), 2), width = _a[0], height = _a[1];
    var combinePen = {
        id: s8(),
        name: 'combine',
        x: selfProperty.x,
        y: selfProperty.y,
        locked: selfProperty.locked,
        width: width,
        height: height,
        children: [],
    };
    pens.push(combinePen);
    children.forEach(function (child) {
        var e_1, _a;
        var pen = undefined;
        var childProperty = child[selfName];
        if (childProperty && childProperty.transform) {
            child.g &&
                child.g.forEach(function (item) {
                    if (!item[selfName]) {
                        item[selfName] = {
                            transform: childProperty.transform,
                        };
                    }
                    else {
                        item[selfName].transform = childProperty.transform;
                    }
                });
        }
        if (child.g) {
            // g 类型
            pen = transformCombines(__assign({ 
                // TODO: g 暂时认为是与父元素一样大
                x: 0, y: 0, width: 1, height: 1, locked: 10 }, childProperty), child.g);
        }
        else if (child.defs) {
            setStyle(child.defs.filter(function (item) { return item.style; }));
            setLinearGradient(child.defs.filter(function (item) { return item.linearGradient; }));
        }
        else if (child.style) {
            setStyle([{ style: child.style }]);
        }
        else if (childProperty) {
            pen = transformNormalShape(childProperty, selfProperty, combinePen.id);
            if (child.path) {
                // path 类型
                pen = transformPath(childProperty, pen);
            }
            else if (child.rect) {
                // rect 类型
                pen = transformRect(childProperty, pen);
                var rectangle = pen;
                //获取圆上右下左坐标
                var circleAnchors = [
                    {
                        x: rectangle.x + rectangle.width * 0.5,
                        y: rectangle.y,
                    },
                    {
                        x: rectangle.x + rectangle.width * 1,
                        y: rectangle.y + rectangle.height * 0.5,
                    },
                    {
                        x: rectangle.x + rectangle.width * 0.5,
                        y: rectangle.y + rectangle.height * 1,
                    },
                    {
                        x: rectangle.x,
                        y: rectangle.y + rectangle.height * 0.5,
                    },
                ];
                circleAnchors.forEach(function (item) {
                    if (item.x <= 0.01 ||
                        item.x >= 0.99 ||
                        item.y <= 0.01 ||
                        item.y >= 0.99) {
                        anchorsArr.push(item);
                    }
                });
            }
            else if (child.circle) {
                // circle 类型
                pen = transformCircle(childProperty, pen);
                var circle = pen;
                //获取圆上右下左坐标
                var circleAnchors = [
                    {
                        x: circle.x + circle.width * 0.5,
                        y: circle.y,
                    },
                    {
                        x: circle.x + circle.width * 1,
                        y: circle.y + circle.height * 0.5,
                    },
                    {
                        x: circle.x + circle.width * 0.5,
                        y: circle.y + circle.height * 1,
                    },
                    {
                        x: circle.x,
                        y: circle.y + circle.height * 0.5,
                    },
                ];
                circleAnchors.forEach(function (item) {
                    if (item.x <= 0.01 ||
                        item.x >= 0.99 ||
                        item.y <= 0.01 ||
                        item.y >= 0.99) {
                        anchorsArr.push(item);
                    }
                });
            }
            else if (child.ellipse) {
                // ellipse 类型
                pen = transformCircle(childProperty, pen);
            }
            else if (child.line) {
                // line 类型
                pen = transformLine(childProperty, pen);
                var line_1 = pen;
                line_1.anchors.forEach(function (item) {
                    var x = line_1.x + item.x * line_1.width;
                    var y = line_1.y + item.y * line_1.height;
                    if (x < 0.01 || x > 0.99 || y < 0.01 || y > 0.99) {
                        //TODO 连线太多的问题
                        anchorsArr.push({
                            x: x,
                            y: y,
                        });
                    }
                });
            }
            else if (child.polygon) {
                // polygon 类型
                pen = transformPolygon(childProperty, pen);
            }
            else if (child.polyline) {
                // polyline 类型
                pen = transformPolyline(childProperty, pen);
                var polyline_1 = pen;
                polyline_1.anchors.forEach(function (item) {
                    var x = polyline_1.x + item.x * polyline_1.width;
                    var y = polyline_1.y + item.y * polyline_1.height;
                    if (x < 0.01 || x > 0.99 || y < 0.01 || y > 0.99) {
                        anchorsArr.push({
                            x: x,
                            y: y,
                        });
                    }
                });
            }
            else if (child.text) {
                // text 类型
                pen = transformText(childProperty, child.text, pen);
            }
            else if (child.image) {
                // image 类型
                pen = transformImage(childProperty, pen);
                // TODO: use 类型节点暂不考虑
            }
            else {
                pen = undefined;
            }
        }
        if (pen) {
            // 此处 pen 在 g 的 情况下可能是个数组
            if (Array.isArray(pen)) {
                try {
                    for (var pen_1 = __values(pen), pen_1_1 = pen_1.next(); !pen_1_1.done; pen_1_1 = pen_1.next()) {
                        var iPen = pen_1_1.value;
                        if (!iPen.parentId) {
                            iPen.parentId = combinePen.id;
                            combinePen.children.push(iPen.id);
                        }
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (pen_1_1 && !pen_1_1.done && (_a = pen_1.return)) _a.call(pen_1);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
                pens.push.apply(pens, __spreadArray([], __read(pen), false));
            }
            else {
                combinePen.children.push(pen.id);
                pens.push(pen);
            }
        }
    });
    return pens;
}
function getTranslate(transform) {
    var offsetX = 0;
    var offsetY = 0;
    if (transform) {
        var matchArr = transform
            .replace('translate(', '')
            .replace(')', '')
            .split(',');
        offsetX = parseFloat(matchArr[0]);
        offsetY = parseFloat(matchArr[1]);
    }
    return {
        offsetX: offsetX,
        offsetY: offsetY,
    };
}
function transformPath(path, pen) {
    var d = path.d;
    if (!d) {
        return;
    }
    var commands = parseSvgPath(d);
    var rect = getRect(commands);
    var _a = getTranslate(path.transform), offsetX = _a.offsetX, offsetY = _a.offsetY;
    rect.x += offsetX;
    rect.ex += offsetX;
    rect.y += offsetY;
    rect.ey += offsetY;
    var x = (rect.x + pen.x - allRect.x) / allRect.width;
    var y = (rect.y + pen.y - allRect.y) / allRect.height;
    var width = rect.width / allRect.width <= 1 ? rect.width / allRect.width : 1;
    var height = rect.height / allRect.height <= 1 ? rect.height / allRect.height : 1;
    var pathPen = __assign(__assign({}, pen), { name: 'svgPath', pathId: s8(), path: d, x: x, y: y, width: width, height: height, disableAnchor: true });
    return pathPen;
}
function transformRect(rect, pen) {
    var x = (rect.x || 0 - allRect.x + pen.x) / allRect.width;
    var y = (rect.y || 0 - allRect.y + pen.y) / allRect.height;
    var width = rect.width / allRect.width <= 1 ? rect.width / allRect.width : 1;
    var height = rect.height / allRect.height <= 1 ? rect.height / allRect.height : 1;
    var borderRadius = rect.rx / rect.width || rect.ry / rect.height || 0;
    var rectPen = __assign(__assign({}, pen), { name: 'rectangle', x: x, y: y, width: width, height: height, borderRadius: borderRadius });
    return rectPen;
}
/**
 * 转换圆形或者椭圆标签成为 circle
 * @param circle 圆形或者椭圆
 * @returns
 */
function transformCircle(circle, pen) {
    var rx = circle.rx || circle.r;
    var ry = circle.ry || circle.r;
    var x = (circle.cx - rx - allRect.x + pen.x) / allRect.width;
    var y = (circle.cy - ry - allRect.y + pen.y) / allRect.height;
    var width = (2 * rx) / allRect.width;
    var height = (2 * ry) / allRect.height;
    return __assign(__assign({}, pen), { name: 'circle', x: x, y: y, width: width, height: height });
}
function transformContainerRect(mySelf) {
    if (mySelf.viewBox) {
        var viewBox = mySelf.viewBox.split(' ');
        return {
            x: Number(viewBox[0]),
            y: Number(viewBox[1]),
            width: Number(viewBox[2]),
            height: Number(viewBox[3]),
        };
    }
    else {
        return {
            x: 0,
            y: 0,
            width: parseFloat(mySelf.width),
            height: parseFloat(mySelf.height),
        };
    }
}
function transformNormalShape(childProperty, parentProperty, parentId) {
    var _a;
    // style > class > self
    var childClassJs = getClassStyle(childProperty.class);
    var parentClassJs = getClassStyle(parentProperty.class);
    // childProperty 的优先级更高
    // style 的优先级更高
    var chileStyleJs = styleToJson(childProperty.style);
    var parentStyleJs = styleToJson(parentProperty.style);
    var finalProperty = __assign(__assign(__assign(__assign(__assign(__assign({}, parentProperty), parentClassJs), parentStyleJs), childProperty), childClassJs), chileStyleJs);
    var background;
    if (finalProperty.fill === 'none') {
    }
    else if ((_a = finalProperty.fill) === null || _a === void 0 ? void 0 : _a.includes('url')) {
        var id_1 = finalProperty.fill.replace('url(#', '').replace(')', '');
        var gradientColor_1 = linearGradient.find(function (item) { return item.id === id_1; });
        if (gradientColor_1 && !gradientColor_1.color) {
            // 颜色不存在，则查找父级
            gradientColor_1 = linearGradient.find(function (item) { return gradientColor_1.from === "#" + item.id; });
        }
        background = gradientColor_1 === null || gradientColor_1 === void 0 ? void 0 : gradientColor_1.color;
    }
    else {
        background = finalProperty.fill;
        // fill 属性不是 none ，是 undefined ，用默认黑色
        !background && (background = '#000');
    }
    var x = 0, y = 0;
    var rotate = 0;
    var matrix = null;
    if (finalProperty.transform) {
        var transforms = finalProperty.transform.split(') ');
        transforms.forEach(function (transform) {
            var _a = __read(transform.split('('), 2), type = _a[0], value = _a[1];
            var _b = __read(value.split(' '), 2), offsetX = _b[0], offsetY = _b[1];
            if (type === 'translate') {
                // 平移
                x = Number(offsetX) || 0;
                y = Number(offsetY) || 0;
            }
            else if (type === 'rotate') {
                // TODO: transform 中的 rotate 圆心与 meta2d.js 圆心不一致，处理过程中默认把 translate 干掉
                // 旋转
                rotate = parseFloat(value);
                x = 0;
                y = 0;
            }
            else if (type === 'matrix') {
                // 矩阵变换
                // TODO: 是否有 , 分隔的？如果有 ，getTranslate 的代码是否重复了？直接挂到这个里面？
                matrix = value.split(' ').map(function (item) { return parseFloat(item); });
            }
        });
    }
    return {
        id: s8(),
        locked: 10,
        parentId: parentId,
        x: x,
        y: y,
        rotate: rotate,
        // TODO: background 可以为空
        background: background,
        color: finalProperty.stroke,
        lineWidth: finalProperty['stroke-width']
            ? parseFloat(finalProperty['stroke-width']) * shapeScale
            : finalProperty.stroke
                ? 1
                : 0,
        lineCap: finalProperty.strokeLinecap,
        lineJoin: finalProperty.strokeLinejoin,
        lineDash: finalProperty.strokeDasharray,
        lineDashOffset: finalProperty.strokeDashoffset,
        // strokeMiterlimit: path.strokeMiterlimit,
        globalAlpha: Number(finalProperty.opacity),
        // transform: path.transform,
        fontSize: finalProperty['font-size']
            ? parseFloat(finalProperty['font-size']) * shapeScale
            : 16,
        fontFamily: finalProperty['font-family'],
        fontWeight: finalProperty['font-weight'],
        fontStyle: finalProperty['font-style'],
        matrix: matrix,
    };
}
/**
 * 由于 points 存在 不使用 , 分割的特例（全部使用 ' '）
 * @param points 字符串 points
 * @returns
 */
function polygonPointsToXY(points) {
    var _points = points.replace(/[\'\"\\\/\b\f\n\r\t]/g, '');
    var pointsArr = _points.split(/\s|,/);
    var resPoints = [];
    pointsArr.forEach(function (item, index) {
        if (index % 2 === 0) {
            resPoints.push({
                x: Number(item),
                y: Number(pointsArr[index + 1]),
            });
        }
    });
    return resPoints;
}
function transformPolygon(childProperty, pen) {
    var pointsStr = childProperty.points;
    var points = polygonPointsToXY(pointsStr);
    var rect = getRectOfPoints(points);
    var anchors = points.map(function (point) {
        return {
            x: (point.x - rect.x) / rect.width,
            y: (point.y - rect.y) / rect.height,
            id: s8(),
        };
    });
    return __assign(__assign({}, pen), { name: 'line', lineName: 'line', type: 0, close: true, anchors: anchors, x: (rect.x - allRect.x + pen.x) / allRect.width, y: (rect.y - allRect.y + pen.y) / allRect.height, width: rect.width / allRect.width, height: rect.height / allRect.height });
}
/**
 * 根据矩阵计算点的坐标
 */
function calcPointByMatrix(point, matrix) {
    var _a = __read(matrix, 6), a = _a[0], b = _a[1], c = _a[2], d = _a[3], e = _a[4], f = _a[5];
    var oldX = point.x;
    var oldY = point.y;
    var x = a * oldX + c * oldY + e;
    var y = b * oldX + d * oldY + f;
    return [x, y];
}
function transformLine(childProperty, pen) {
    var _a, _b;
    var x1 = Number(childProperty.x1) || 0;
    var x2 = Number(childProperty.x2) || 0;
    var y1 = Number(childProperty.y1) || 0;
    var y2 = Number(childProperty.y2) || 0;
    if (Array.isArray(pen.matrix)) {
        _a = __read(calcPointByMatrix({ x: x1, y: y1 }, pen.matrix), 2), x1 = _a[0], y1 = _a[1];
        _b = __read(calcPointByMatrix({ x: x2, y: y2 }, pen.matrix), 2), x2 = _b[0], y2 = _b[1];
    }
    var minX = Math.min(x1, x2);
    var maxX = Math.max(x1, x2);
    var minY = Math.min(y1, y2);
    var maxY = Math.max(y1, y2);
    var x = (minX - allRect.x + pen.x) / allRect.width;
    var y = (minY - allRect.y + pen.y) / allRect.height;
    var width = (maxX - minX) / allRect.width;
    var height = (maxY - minY) / allRect.height;
    return __assign(__assign({}, pen), { name: 'line', lineName: 'line', type: 0, x: x, y: y, width: width, height: height, anchors: [
            {
                x: x1 < x2 ? 0 : 1,
                y: y1 < y2 ? 0 : 1,
                hidden: true,
                id: s8(),
            },
            {
                x: x1 < x2 ? 1 : 0,
                y: y1 < y2 ? 1 : 0,
                hidden: true,
                id: s8(),
            },
        ] });
}
function transformPolyline(childProperty, pen) {
    // 多段直线
    var polyline = transformPolygon(childProperty, pen);
    return Object.assign(polyline, {
        close: false,
        type: 1,
        anchors: polyline.anchors.map(function (anchor) {
            anchor.hidden = true;
            return anchor;
        }),
    });
}
function transformText(childProperty, textContent, pen) {
    var _a;
    // 文字
    var text = (_a = textContent[0]) === null || _a === void 0 ? void 0 : _a[contentProp];
    var width = measureText(text, pen);
    var height = 0; // pen.fontSize / shapeScale;
    //TODO 这里为什么需要减去height
    var _b = getTranslate(childProperty.transform), offsetX = _b.offsetX, offsetY = _b.offsetY;
    return __assign(__assign({}, pen), { name: 'text', textAlign: 'left', text: text, x: (parseFloat(childProperty.x) + (offsetX || 0) - allRect.x + pen.x) /
            allRect.width, y: (parseFloat(childProperty.y) +
            (offsetY || 0) -
            allRect.y +
            pen.y -
            height) /
            allRect.height, width: width / allRect.width, height: height / allRect.height });
}
function transformImage(childProperty, pen) {
    var x = childProperty.x, y = childProperty.y, width = childProperty.width, height = childProperty.height;
    return __assign(__assign({}, pen), { name: 'image', x: (x - allRect.x + pen.x) / allRect.width, y: (y - allRect.y + pen.y) / allRect.height, width: width / allRect.width, height: height / allRect.height, image: childProperty['xlink:href'] });
}
// TODO: 粗糙计算文字宽度
function measureText(text, pen) {
    var fontSize = pen.fontSize || 16;
    return (fontSize / shapeScale) * String(text).length;
}
function styleToJson(style) {
    if (!style) {
        return {};
    }
    var styleArr = style.split(';');
    var json = {};
    styleArr.forEach(function (item) {
        var _a = __read(item.split(':'), 2), key = _a[0], value = _a[1];
        key && (json[key] = value);
    });
    return json;
}
var contentProp = '#text';
var style = undefined;
function setStyle(defs) {
    defs.forEach(function (def) {
        if (def.style && def.style[0]) {
            // TODO: 暂时只支持一个 style 的情况
            style = cssToJson(def.style[0][contentProp]);
        }
    });
}
// TODO: 渐变中目前只存储一个颜色 渐变
var linearGradient = [];
function setLinearGradient(defs) {
    // 此处的 defs 数组是只包含 linearGradient 属性的
    defs.forEach(function (def) {
        if (def.linearGradient) {
            var linearGradientItem = {
                id: def[selfName].id,
                from: def[selfName]['xlink:href'],
                color: undefined,
            };
            if (def.linearGradient.length > 0) {
                linearGradientItem.color =
                    def.linearGradient[0][selfName]['stop-color'];
            }
            linearGradient.push(linearGradientItem);
        }
    });
}
function cssToJson(text) {
    var json = {};
    var styleArr = text.split('}');
    styleArr.forEach(function (item) {
        var _a = __read(item.split('{'), 2), key = _a[0], value = _a[1];
        key && (json[key.trim()] = styleToJson(value));
    });
    return json;
}
function getClassStyle(className) {
    var classStyle = {};
    for (var key in style) {
        if (Object.prototype.hasOwnProperty.call(style, key)) {
            var value = style[key];
            if (key === '.' + className) {
                Object.assign(classStyle, value);
            }
        }
    }
    return classStyle;
}
/**
 * 处理单位，目前只考虑到 in 英寸 ，其它的返回原值
 * @param selfProperty 当前元素的属性
 * @returns
 */
function dealUnit(selfProperty) {
    var _a;
    if ((_a = String(selfProperty.width)) === null || _a === void 0 ? void 0 : _a.endsWith('in')) {
        // 英寸
        var width_1 = parseFloat(selfProperty.width) * 96;
        var height_1 = parseFloat(selfProperty.height) * 96;
        return [width_1, height_1];
    }
    var width = parseFloat(selfProperty.width) || 0;
    var height = parseFloat(selfProperty.height) || 0;
    return [width, height];
}
//# sourceMappingURL=svg.js.map