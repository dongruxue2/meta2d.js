import { table } from './table';
import { le5leSwitch } from './switch';
import { slider } from './slider';
import { checkbox } from './checkbox';
import { radio } from './radio';
import { table2 } from './table2';
import { time } from './time';
export declare function formPens(): {
    radio: typeof radio;
    switch: typeof le5leSwitch;
    slider: typeof slider;
    checkbox: typeof checkbox;
    table: typeof table;
    table2: typeof table2;
};
export declare function formPath2DPens(): {
    time: typeof time;
};
