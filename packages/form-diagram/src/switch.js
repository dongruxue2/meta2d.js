export function le5leSwitch(ctx, pen) {
    if (!pen.onClick) {
        pen.onClick = click;
    }
    var x = pen.calculative.worldRect.x;
    var y = pen.calculative.worldRect.y;
    var w = pen.calculative.worldRect.width;
    var h = pen.calculative.worldRect.height;
    if (w < h * 1.5) {
        w = 1.5 * h;
    }
    ctx.beginPath();
    ctx.arc(x + h / 2, y + h / 2, h / 2, Math.PI / 2, (Math.PI * 3) / 2);
    ctx.lineTo(x + w - h / 2, y);
    ctx.arc(x + w - h / 2, y + h / 2, h / 2, -Math.PI / 2, Math.PI / 2);
    ctx.lineTo(x + h / 2, y + h);
    if (pen.checked) {
        ctx.fillStyle = pen.onColor;
        if (pen.disable) {
            ctx.fillStyle = pen.disableOnColor;
        }
        ctx.fill();
        ctx.closePath();
        ctx.beginPath();
        ctx.fillStyle = '#ffffff';
        ctx.moveTo(x + h * 2, y + h / 2);
        ctx.arc(x + w - h / 2, y + h / 2, h / 2 > 2 ? h / 2 - 2 : 1, 0, Math.PI * 2);
        ctx.fill();
    }
    else {
        ctx.fillStyle = pen.offColor;
        if (pen.disable) {
            ctx.fillStyle = pen.disableOffColor;
        }
        ctx.fill();
        ctx.closePath();
        ctx.beginPath();
        ctx.fillStyle = '#ffffff';
        ctx.moveTo(x + h, y + h / 2);
        ctx.arc(x + h / 2, y + h / 2, h / 2 > 2 ? h / 2 - 2 : 1, 0, Math.PI * 2);
        ctx.fill();
    }
    ctx.closePath();
}
function click(pen) {
    if (pen.disable) {
        return;
    }
    pen.checked = !pen.checked;
    pen.calculative.canvas.store.emitter.emit('valueUpdate', pen);
    pen.calculative.canvas.render();
}
//# sourceMappingURL=switch.js.map