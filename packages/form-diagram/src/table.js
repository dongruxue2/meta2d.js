var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
import { calcRightBottom, calcTextLines } from '@meta2d/core';
export function table(ctx, pen) {
    if (!pen.onAdd) {
        pen.onAdd = onAdd;
        pen.onMouseMove = onMouseMove;
        pen.onMouseLeave = onMouseLeave;
        pen.onMouseDown = onMouseDown;
        pen.onShowInput = onShowInput;
        pen.onInput = onInput;
        pen.onValue = onValue;
        pen.onBeforeValue = beforeValue;
    }
    var data = pen.calculative.canvas.store.data;
    var options = pen.calculative.canvas.store.options;
    pen.color = pen.color || data.color || options.color;
    pen.activeColor = pen.activeColor || options.activeColor;
    pen.hoverColor = pen.hoverColor || options.hoverColor;
    pen.activeBackground = pen.activeBackground || options.activeBackground;
    pen.hoverBackground = pen.hoverBackground || options.hoverBackground;
    // 画网格线
    drawGridLine(ctx, pen);
    // 画单元格
    drawCell(ctx, pen);
}
function initRect(pen) {
    var e_1, _a, e_2, _b;
    var colPos = [];
    var rowPos = [];
    if (!pen.table.rowHeight) {
        pen.table.rowHeight = 40;
    }
    if (!pen.table.colWidth) {
        pen.table.colWidth = 150;
    }
    var width = 0;
    try {
        for (var _c = __values(pen.table.header.data), _d = _c.next(); !_d.done; _d = _c.next()) {
            var item = _d.value;
            width += item.width || pen.table.colWidth;
            colPos.push(width);
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
        }
        finally { if (e_1) throw e_1.error; }
    }
    var height = 0;
    // 显示表头
    if (pen.table.header.show != false) {
        height += pen.table.header.height || pen.table.rowHeight;
        rowPos.push(height);
    }
    try {
        for (var _e = __values(pen.table.data), _f = _e.next(); !_f.done; _f = _e.next()) {
            var item = _f.value;
            height += item.height || pen.table.rowHeight;
            rowPos.push(height);
        }
    }
    catch (e_2_1) { e_2 = { error: e_2_1 }; }
    finally {
        try {
            if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
        }
        finally { if (e_2) throw e_2.error; }
    }
    pen.colPos = colPos;
    pen.rowPos = rowPos;
    pen.tableWidth = width;
    pen.tableHeight = height;
    if (!pen.width) {
        pen.width = width;
        pen.height = height;
        pen.calculative.width = width;
        pen.calculative.height = height;
        pen.calculative.worldRect = {
            x: pen.x,
            y: pen.y,
            height: pen.height,
            width: pen.width,
        };
        calcRightBottom(pen.calculative.worldRect);
    }
}
function drawGridLine(ctx, pen) {
    var e_3, _a;
    if (!pen.colPos) {
        return;
    }
    var worldRect = pen.calculative.worldRect;
    ctx.save();
    ctx.strokeStyle = pen.color;
    // 绘画最外框
    ctx.beginPath();
    ctx.rect(worldRect.x, worldRect.y, worldRect.width, worldRect.height);
    if (pen.background) {
        ctx.fillStyle = pen.background;
        ctx.fill();
    }
    ctx.stroke();
    // 绘画行的线
    var last = pen.rowPos[pen.rowPos.length - 1];
    try {
        for (var _b = __values(pen.rowPos), _c = _b.next(); !_c.done; _c = _b.next()) {
            var item = _c.value;
            if (item === last) {
                continue;
            }
            var y = (item * pen.calculative.worldRect.height) / pen.tableHeight;
            ctx.beginPath();
            ctx.moveTo(pen.calculative.worldRect.x, pen.calculative.worldRect.y + y);
            ctx.lineTo(pen.calculative.worldRect.ex, pen.calculative.worldRect.y + y);
            ctx.stroke();
        }
    }
    catch (e_3_1) { e_3 = { error: e_3_1 }; }
    finally {
        try {
            if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
        }
        finally { if (e_3) throw e_3.error; }
    }
    // 绘画列的线
    last = pen.colPos[pen.colPos.length - 1];
    pen.colPos.forEach(function (item, i) {
        if (item === last) {
            return;
        }
        var x = (item * pen.calculative.worldRect.width) / pen.tableWidth;
        ctx.beginPath();
        ctx.moveTo(pen.calculative.worldRect.x + x, pen.calculative.worldRect.y);
        ctx.lineTo(pen.calculative.worldRect.x + x, pen.calculative.worldRect.ey);
        ctx.stroke();
    });
    ctx.restore();
}
function drawCell(ctx, pen) {
    var _a, _b, _c, _d;
    if (!pen.colPos) {
        return;
    }
    if (!pen.calculative.texts) {
        pen.calculative.texts = [];
    }
    // const textScale = Math.min(
    //   pen.calculative.worldRect.width / pen.tableWidth,
    //   pen.calculative.worldRect.height / pen.tableHeight
    // );
    var textScale = 1;
    for (var i = 0; i < pen.rowPos.length; i++) {
        var _loop_1 = function (j) {
            var cell = getCell(pen, i, j);
            var color = cell.color || pen.color;
            var background = cell.background;
            var activeColor = void 0;
            // 选中
            if (((_a = pen.calculative.activeCell) === null || _a === void 0 ? void 0 : _a.row) === i &&
                ((_b = pen.calculative.activeCell) === null || _b === void 0 ? void 0 : _b.col) === j) {
                color = pen.activeColor;
                background = pen.activeBackground;
                activeColor = color;
            }
            // hover
            if (((_c = pen.calculative.hoverCell) === null || _c === void 0 ? void 0 : _c.row) === i &&
                ((_d = pen.calculative.hoverCell) === null || _d === void 0 ? void 0 : _d.col) === j) {
                color = pen.hoverColor;
                background = pen.hoverBackground;
                activeColor = color;
            }
            var rect = getCellRect(pen, i, j);
            // 有背景
            if (background) {
                ctx.save();
                ctx.fillStyle = background;
                ctx.fillRect(rect.x, rect.y, rect.width, rect.height);
                ctx.restore();
            }
            // 选中或hover
            if (activeColor) {
                ctx.save();
                ctx.strokeStyle = activeColor;
                ctx.strokeRect(rect.x, rect.y, rect.width, rect.height);
                ctx.restore();
            }
            // 绘画文本
            pen.calculative.worldTextRect = rect;
            var rowText = pen.calculative.texts[i];
            if (!pen.calculative.texts[i]) {
                rowText = [];
                pen.calculative.texts.push(rowText);
            }
            if (rowText[j] == null) {
                if (Array.isArray(cell)) {
                    rowText[j] = '';
                    //子节点创建后无需再计算位置
                    if (!cell[0].id) {
                        calcChildrenRect(pen, rect, cell);
                        pen.calculative.canvas.parent.pushChildren(pen, cell);
                    }
                    return "continue";
                }
                else {
                    rowText[j] = cell.text || cell + '';
                }
                if (!rowText[j]) {
                    return "continue";
                }
                // 计算换行和省略号
                rowText[j] = calcTextLines(pen, rowText[j]);
            }
            if (!rowText[j]) {
                return "continue";
            }
            ctx.save();
            ctx.fillStyle = color;
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            ctx.font =
                (pen.calculative.fontStyle || '') +
                    ' normal ' +
                    (pen.calculative.fontWeight || '') +
                    ' ' +
                    (pen.calculative.fontSize || 12) * textScale +
                    'px ' +
                    pen.calculative.fontFamily;
            if (rowText[j].length === 1) {
                ctx.fillText(rowText[j][0], rect.x + rect.width / 2, rect.y + rect.height / 2);
            }
            else {
                var y_1 = 0.55;
                var lineHeight_1 = pen.calculative.fontSize * pen.calculative.lineHeight * textScale;
                var h = rowText[j].length * lineHeight_1;
                var top_1 = (rect.height - h) / 2;
                rowText[j].forEach(function (text, i) {
                    ctx.fillText(text, rect.x + rect.width / 2, rect.y + top_1 + (i + y_1) * lineHeight_1);
                });
            }
            ctx.restore();
        };
        for (var j = 0; j < pen.colPos.length; j++) {
            _loop_1(j);
        }
    }
}
// 添加table节点回调
function onAdd(pen) {
    initRect(pen);
}
function onShowInput(pen, e) {
    // 没有活动单元格
    if (!pen.calculative.hoverCell) {
        return;
    }
    var cell = getCell(pen, pen.calculative.hoverCell.row, pen.calculative.hoverCell.col);
    // 子节点，非文本
    if (Array.isArray(cell)) {
        return;
    }
    pen.calculative.inputCell = pen.calculative.hoverCell;
    var rect = getCellRect(pen, pen.calculative.hoverCell.row, pen.calculative.hoverCell.col);
    pen.calculative.tempText = cell.text || cell + '';
    pen.calculative.canvas.showInput(pen, rect, '#ffffff');
}
//将输入的数据写入到对应的data中
function onInput(pen, text) {
    if (!pen.calculative.inputCell) {
        return;
    }
    setCellText(pen, pen.calculative.inputCell.row, pen.calculative.inputCell.col, text);
    pen.calculative.canvas.render();
}
function onMouseMove(pen, e) {
    pen.calculative.hoverCell = getCellIndex(pen, e);
    pen.calculative.canvas.render();
}
function onMouseLeave(pen, e) {
    pen.calculative.hoverCell = undefined;
    pen.calculative.canvas.render();
}
function onMouseDown(pen, e) {
    pen.calculative.activeCell = getCellIndex(pen, e);
    pen.calculative.canvas.render();
}
// 根据坐标，计算在哪个cell
function getCellIndex(pen, e) {
    var scaleX = pen.calculative.worldRect.width / pen.tableWidth;
    var scaleY = pen.calculative.worldRect.height / pen.tableHeight;
    var pos = { row: 0, col: 0 };
    for (var i = 0; i < pen.colPos.length; i++) {
        if (e.x > pen.calculative.worldRect.x + pen.colPos[i] * scaleX) {
            pos.col = i + 1;
        }
    }
    for (var i = 0; i < pen.rowPos.length; i++) {
        if (e.y > pen.calculative.worldRect.y + pen.rowPos[i] * scaleY) {
            pos.row = i + 1;
        }
    }
    return pos;
}
// 根据index获取cell
function getCell(pen, rowIndex, colIndex) {
    if (!pen.table.data || !Array.isArray(pen.table.data)) {
        return;
    }
    if (pen.table.header.show == false) {
        var row_1 = pen.table.data[rowIndex];
        if (Array.isArray(row_1)) {
            return row_1[colIndex];
        }
        else if (!row_1.data || !Array.isArray(row_1.data)) {
            return;
        }
        return row_1.data[colIndex];
    }
    // 显示表头
    if (rowIndex === 0) {
        var cell = pen.table.header.data[colIndex];
        cell.fontWeight = pen.table.header.fontWeight;
        return cell;
    }
    var row = pen.table.data[rowIndex - 1];
    if (!row) {
        return;
    }
    else if (Array.isArray(row)) {
        return row[colIndex];
    }
    else if (!row.data || !Array.isArray(row.data)) {
        return;
    }
    return row.data[colIndex];
}
// 设置cell的文本
function setCellText(pen, rowIndex, colIndex, text) {
    if (!pen.table.data || !Array.isArray(pen.table.data)) {
        return;
    }
    //TODO 导致错误
    pen.calculative.texts = undefined;
    var rowData;
    // 没有表头
    if (pen.table.header.show == false) {
        rowData = pen.table.data[rowIndex];
        if (Array.isArray(rowData)) {
            // data: [[1,2,3],[a,b,c]]
        }
        else if (rowData.data && Array.isArray(rowData.data)) {
            // data: [{data:[1,2,3]}, {data:[1,2,3]}]
            rowData = rowData.data;
        }
    }
    else {
        // 有表头
        if (rowIndex === 0) {
            rowData = pen.table.header.data;
        }
        else {
            rowData = pen.table.data[rowIndex - 1];
            if (Array.isArray(rowData)) {
                // data: [[1,2,3],[a,b,c]]
            }
            else if (rowData.data && Array.isArray(rowData.data)) {
                // data: [{data:[1,2,3]}, {data:[1,2,3]}]
                rowData = rowData.data;
            }
        }
    }
    if (!rowData) {
        return;
    }
    if (rowData[colIndex] instanceof Object) {
        rowData[colIndex].text = text;
    }
    else {
        rowData[colIndex] = text;
    }
    pen.calculative.canvas.store.emitter.emit('valueUpdate', pen);
}
// 计算cell世界坐标区域
function getCellRect(pen, rowIndex, colIndex) {
    var scaleX = pen.calculative.worldRect.width / pen.tableWidth;
    var scaleY = pen.calculative.worldRect.height / pen.tableHeight;
    var x = 0;
    var ex = pen.colPos[colIndex] * scaleX;
    if (colIndex > 0) {
        x = pen.colPos[colIndex - 1] * scaleX;
    }
    var y = 0;
    var ey = pen.rowPos[rowIndex] * scaleY;
    if (rowIndex > 0) {
        y = pen.rowPos[rowIndex - 1] * scaleY;
    }
    return {
        x: pen.calculative.worldRect.x + x,
        y: pen.calculative.worldRect.y + y,
        ex: pen.calculative.worldRect.x + ex,
        ey: pen.calculative.worldRect.y + ey,
        width: ex - x,
        height: ey - y,
    };
}
// 计算cell子节点的世界坐标区域
function calcChildrenRect(pen, rect, children) {
    var e_4, _a, e_5, _b;
    var scaleX = pen.calculative.worldRect.width / pen.tableWidth;
    var scaleY = pen.calculative.worldRect.height / pen.tableHeight;
    // 计算子节点需要的宽高
    var height = 0;
    var lastX = 0;
    var lastY = 0;
    try {
        for (var children_1 = __values(children), children_1_1 = children_1.next(); !children_1_1.done; children_1_1 = children_1.next()) {
            var item = children_1_1.value;
            if (lastX + item.width * scaleX + 20 * scaleX < rect.width) {
                item.x = rect.x + lastX + 10 * scaleX;
                item.y = rect.y + lastY + 10 * scaleY;
                lastX += (item.width + 10) * scaleX;
                height = Math.max(height, lastY + (item.height + 10) * scaleY);
            }
            else {
                // 超出需要换行
                lastX = 0;
                lastY = height;
                item.x = rect.x + lastX + 10 * scaleX;
                item.y = rect.y + lastY + 10 * scaleY;
                height += (item.height + 10) * scaleY;
            }
        }
    }
    catch (e_4_1) { e_4 = { error: e_4_1 }; }
    finally {
        try {
            if (children_1_1 && !children_1_1.done && (_a = children_1.return)) _a.call(children_1);
        }
        finally { if (e_4) throw e_4.error; }
    }
    // 垂直居中
    if (height + 20 * scaleY < rect.height) {
        var top_2 = (rect.height - height - 10 * scaleY) / 2;
        try {
            for (var children_2 = __values(children), children_2_1 = children_2.next(); !children_2_1.done; children_2_1 = children_2.next()) {
                var item = children_2_1.value;
                item.y += top_2;
            }
        }
        catch (e_5_1) { e_5 = { error: e_5_1 }; }
        finally {
            try {
                if (children_2_1 && !children_2_1.done && (_b = children_2.return)) _b.call(children_2);
            }
            finally { if (e_5) throw e_5.error; }
        }
    }
}
function onValue(pen) {
    pen.calculative.texts = undefined;
}
function beforeValue(pen, value) {
    if (value.table ||
        (value.col == undefined && value.row == undefined)) {
        // 整体传参，不做处理
        return value;
    }
    setCellText(pen, value.row, value.col, value.value);
    pen.calculative.canvas.render();
    delete value.col;
    delete value.row;
    return value;
}
//# sourceMappingURL=table.js.map