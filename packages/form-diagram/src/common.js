import { calcRightBottom } from '@meta2d/core';
export var ReplaceMode;
(function (ReplaceMode) {
    ReplaceMode[ReplaceMode["Add"] = 0] = "Add";
    ReplaceMode[ReplaceMode["Replace"] = 1] = "Replace";
    ReplaceMode[ReplaceMode["ReplaceAll"] = 2] = "ReplaceAll";
})(ReplaceMode || (ReplaceMode = {}));
export function getTextLength(text, pen) {
    var textScale = (pen.calculative.worldRect.height * 14) / 16;
    var chinese = text.match(/[\u4e00-\u9fa5]/g) || '';
    var chineseLen = chinese.length;
    var width = (text.length - chineseLen) * textScale * 0.6 + chineseLen * textScale;
    return width;
}
export function initOptions(pen) {
    if (pen.direction == 'horizontal') {
        var optionPos_1 = [];
        var textLength_1 = 0;
        var h_1 = pen.height;
        pen.checkboxHeight = h_1;
        pen.options.forEach(function (item, index) {
            optionPos_1.push(index * (40 + h_1) + textLength_1);
            textLength_1 += getTextLength(item.text, pen);
        });
        pen.optionPos = optionPos_1;
        var width = optionPos_1.length * (40 + h_1) + textLength_1;
        pen.checkboxWidth = width;
        pen.width = width;
        pen.calculative.width = width;
        pen.calculative.worldRect = {
            x: pen.x,
            y: pen.y,
            height: pen.height,
            width: pen.width,
            center: {
                x: pen.x + pen.width / 2,
                y: pen.y + pen.height / 2,
            },
        };
        calcRightBottom(pen.calculative.worldRect);
    }
    else if (pen.direction == 'vertical') {
        if (pen.optionInterval == undefined) {
            pen.optionInterval = 20;
        }
        if (!pen.optionHeight) {
            pen.optionHeight = 20;
        }
        var optionPos_2 = [];
        pen.options.forEach(function (item, index) {
            optionPos_2.push(index * (pen.optionInterval + pen.optionHeight));
        });
        pen.optionPos = optionPos_2;
        var height = optionPos_2[optionPos_2.length - 1] + pen.optionHeight;
        pen.checkboxHeight = height;
        if (!pen.width) {
            pen.height = height;
            pen.calculative.height = height;
            pen.calculative.worldRect = {
                x: pen.x,
                y: pen.y,
                height: pen.height,
                width: pen.width,
                center: {
                    x: pen.x + pen.width / 2,
                    y: pen.y + pen.height / 2,
                },
            };
            calcRightBottom(pen.calculative.worldRect);
        }
    }
}
//# sourceMappingURL=common.js.map