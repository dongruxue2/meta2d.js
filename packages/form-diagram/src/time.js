export function time(pen, ctx) {
    var path = !ctx ? new Path2D() : ctx;
    var _a = pen.calculative.worldRect, x = _a.x, y = _a.y, width = _a.width, height = _a.height;
    path.rect(x, y, width, height);
    if (!pen.onAdd) {
        pen.onAdd = onAdd;
        pen.onDestroy = onDestroy;
        if (pen.interval) {
            pen.onDestroy(pen);
            pen.onAdd(pen);
        }
    }
    if (path instanceof Path2D)
        return path;
    return;
}
function formatTime(pen) {
    //更多 https://blog.csdn.net/Endeavorseven/article/details/101310628
    var weeks = ['天', '一', '二', '三', '四', '五', '六'];
    var now = new Date();
    var year = now.getFullYear();
    var pad = '';
    if (pen.fillZero) {
        pad = '0';
    }
    var month = (now.getMonth() + 1 + '').padStart(2, pad);
    var day = (now.getDate() + '').padStart(2, pad);
    var week = now.getDay();
    var hours = (now.getHours() + '').padStart(2, pad);
    var minutes = (now.getMinutes() + '').padStart(2, pad);
    var seconds = (now.getSeconds() + '').padStart(2, pad);
    var fn = new Function('year', 'month', 'day', 'week', 'hours', 'minutes', 'seconds', pen.timeFormat
        ? "return " + pen.timeFormat
        : 'return `${year}:${month}:${day} ${hours}:${minutes}:${seconds} 星期${week}`');
    var time = fn(year, month, day, weeks[week], hours, minutes, seconds);
    return time;
}
function onAdd(pen) {
    pen.interval = setInterval(function () {
        var text = formatTime(pen);
        pen.calculative.canvas.parent.setValue({ id: pen.id, text: text }, { history: false, doEvent: false, render: false });
        pen.calculative.canvas.render();
    }, pen.timeout || 1000);
}
function onDestroy(pen) {
    if (pen.interval) {
        clearInterval(pen.interval);
        pen.interval = undefined;
    }
}
//# sourceMappingURL=time.js.map