var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
import { ReplaceMode } from './common';
import { calcRightBottom, calcTextLines } from '../../core';
export function table2(ctx, pen) {
    if (!pen.onAdd) {
        pen.onAdd = onAdd;
        if (!pen.rowPos || !pen.colPos) {
            pen.onAdd(pen);
            // pen.calculative.canvas.parent.active([pen]);
        }
        pen.onMouseMove = onMouseMove;
        pen.onMouseLeave = onMouseLeave;
        pen.onMouseDown = onMouseDown;
        pen.onShowInput = onShowInput;
        pen.onInput = onInput;
        pen.onValue = onValue;
        pen.onBeforeValue = beforeValue;
    }
    var data = pen.calculative.canvas.store.data;
    var options = pen.calculative.canvas.store.options;
    pen.color = pen.color || data.color || options.color;
    pen.textColor =
        pen.textColor || pen.color || data.textColor || options.textColor;
    pen.activeColor = pen.activeColor || options.activeColor;
    pen.hoverColor = pen.hoverColor || options.hoverColor;
    pen.activeBackground = pen.activeBackground || options.activeBackground;
    pen.hoverBackground = pen.hoverBackground || options.hoverBackground;
    // 画网格线
    drawGridLine(ctx, pen);
    // 画单元格
    drawCell(ctx, pen);
    // 画title
    drawNote(ctx, pen);
    pen.isFirstTime = false;
}
function drawNote(ctx, pen) {
    if (!pen.calculative.hover) {
        return;
    }
    if (!pen.calculative.hoverCell) {
        return;
    }
    if (pen.calculative.isInput) {
        return;
    }
    if (!pen.calculative.isHover) {
        return;
    }
    var rect = pen.calculative.worldRect;
    var mousePos = pen.calculative.canvas.mousePos;
    if (!(mousePos.x > rect.x &&
        mousePos.x < rect.x + rect.width &&
        mousePos.y > rect.y &&
        mousePos.y < rect.y + rect.height)) {
        pen.calculative.hover = false;
        pen.calculative.isHover = false;
        pen.calculative.hoverCell = undefined;
        return;
    }
    var _a = pen.calculative.hoverCell, row = _a.row, col = _a.col;
    var _b = pen.calculative.canvas.mousePos, x = _b.x, y = _b.y;
    if (!pen.data[row]) {
        return;
    }
    var text = pen.data[row][col];
    if (typeof text === 'object' || !text) {
        return;
    }
    ctx.save();
    ctx.textAlign = 'start';
    ctx.textBaseline = 'middle';
    ctx.font = ctx.font =
        (pen.calculative.fontStyle || '') +
            ' normal ' +
            (pen.calculative.fontWeight || '') +
            ' ' +
            (pen.calculative.fontSize || 12) +
            'px ' +
            pen.calculative.fontFamily;
    var noteWidth = ctx.measureText(text).width;
    ctx.beginPath();
    ctx.fillStyle = '#fff';
    ctx.strokeStyle = '#000';
    ctx.moveTo(x, y);
    ctx.rect(x - 10, y, noteWidth + 20, 20);
    ctx.fill();
    ctx.stroke();
    ctx.beginPath();
    ctx.fillStyle = '#000';
    ctx.fillText(text, x, y + 10);
    ctx.restore();
}
function initRect(pen) {
    var colPos = [];
    var rowPos = [];
    if (!pen.rowHeight) {
        pen.rowHeight = 40;
    }
    if (!pen.colWidth) {
        pen.colWidth = 150;
    }
    var width = 0;
    //获取所有col width
    var _col = pen.styles &&
        pen.styles.filter(function (item) {
            return item.col !== undefined && item.row === undefined && item.width;
        });
    var _colWidthMap = {};
    _col &&
        _col.forEach(function (_c) {
            _colWidthMap[_c.col] = _c.width;
        });
    for (var i = 0; i < pen.data[0].length; i++) {
        width +=
            (_colWidthMap[i] || pen.colWidth) *
                pen.calculative.canvas.store.data.scale;
        colPos.push(width);
    }
    var height = 0;
    //获取所有row height
    var _row = pen.styles &&
        pen.styles.filter(function (item) {
            return item.col === undefined && item.row !== undefined && item.height;
        });
    var _rowHeightMap = {};
    _row &&
        _row.forEach(function (_r) {
            _rowHeightMap[_r.row] = _r.height;
        });
    // 显示表头
    for (var j = 0; j < pen.data.length; j++) {
        height +=
            (_rowHeightMap[j] || pen.rowHeight) *
                pen.calculative.canvas.store.data.scale;
        rowPos.push(height);
    }
    pen.colPos = colPos;
    pen.rowPos = rowPos;
    pen.tableWidth = width;
    pen.tableHeight = height;
    //   if (!pen.width) {
    pen.width = width;
    pen.height = height;
    pen.calculative.width = width;
    pen.calculative.height = height;
    pen.calculative.worldRect = {
        x: pen.x,
        y: pen.y,
        height: pen.height,
        width: pen.width,
        center: {
            x: pen.x + pen.width / 2,
            y: pen.y + pen.height / 2,
        },
    };
    calcRightBottom(pen.calculative.worldRect);
    //   }
}
function drawGridLine(ctx, pen) {
    var e_1, _a;
    if (!pen.colPos) {
        return;
    }
    // const worldRect = pen.calculative.worldRect;
    var _b = pen.calculative.worldRect, x = _b.x, y = _b.y, width = _b.width, height = _b.height, ex = _b.ex, ey = _b.ey;
    ctx.save();
    ctx.strokeStyle = pen.color;
    // 绘画最外框
    ctx.beginPath();
    // ctx.rect(worldRect.x, worldRect.y, worldRect.width, worldRect.height);
    var wr = pen.calculative.borderRadius || 0, hr = wr;
    if (wr < 1) {
        wr = width * wr;
        hr = height * hr;
    }
    var r = wr < hr ? wr : hr;
    if (width < 2 * r) {
        r = width / 2;
    }
    if (height < 2 * r) {
        r = height / 2;
    }
    ctx.moveTo(x + r, y);
    ctx.arcTo(ex, y, ex, ey, r);
    ctx.arcTo(ex, ey, x, ey, r);
    ctx.arcTo(x, ey, x, y, r);
    ctx.arcTo(x, y, ex, y, r);
    if (pen.background) {
        ctx.fillStyle = pen.background;
        ctx.fill();
    }
    if (pen.bordered !== false) {
        ctx.strokeStyle = pen.borderColor || '#424B61';
        ctx.stroke();
    }
    if (pen.hLine !== false) {
        // 绘画行的线
        var last = pen.rowPos[pen.rowPos.length - 1];
        try {
            for (var _d = __values(pen.rowPos), _e = _d.next(); !_e.done; _e = _d.next()) {
                var item = _e.value;
                if (item === last) {
                    continue;
                }
                var y_1 = (item * pen.calculative.worldRect.height) / pen.tableHeight;
                ctx.beginPath();
                ctx.moveTo(pen.calculative.worldRect.x, pen.calculative.worldRect.y + y_1);
                ctx.lineTo(pen.calculative.worldRect.ex, pen.calculative.worldRect.y + y_1);
                ctx.strokeStyle = pen.borderColor || '#424B61';
                ctx.stroke();
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_e && !_e.done && (_a = _d.return)) _a.call(_d);
            }
            finally { if (e_1) throw e_1.error; }
        }
    }
    if (pen.vLine !== false) {
        // 绘画列的线
        var last_1 = pen.colPos[pen.colPos.length - 1];
        pen.colPos.forEach(function (item, i) {
            if (item === last_1) {
                return;
            }
            var x = (item * pen.calculative.worldRect.width) / pen.tableWidth;
            ctx.beginPath();
            ctx.moveTo(pen.calculative.worldRect.x + x, pen.calculative.worldRect.y);
            ctx.lineTo(pen.calculative.worldRect.x + x, pen.calculative.worldRect.ey);
            ctx.strokeStyle = pen.borderColor || '#424B61';
            ctx.stroke();
        });
    }
    ctx.restore();
}
function drawCell(ctx, pen) {
    var _a, _b, _d, _e;
    if (!pen.colPos) {
        return;
    }
    if (!pen.calculative.texts) {
        pen.calculative.texts = [];
    }
    var textScale = 1;
    var _loop_1 = function (i) {
        var rowStyle = getRow(pen, i).style;
        var _loop_2 = function (j) {
            var _f = getCell(pen, i, j), cell = _f.value, cellStyle = _f.style;
            var isSuccess = true;
            //样式条件成立
            if (cellStyle.wheres &&
                Array.isArray(cellStyle.wheres)) {
                isSuccess = false;
                isSuccess = cellStyle.wheres.every(function (where) {
                    var fn = new Function('attr', "return attr " + where.comparison + " " + where.value);
                    return fn(cell);
                });
            }
            var color = pen.color;
            var textColor = pen.textColor || pen.color;
            var background = null;
            var fontSize = null;
            var fontWeight = null;
            var fontStyle = null;
            if (isSuccess) {
                color =
                    cellStyle.color || rowStyle.color || pen.color;
                textColor =
                    cellStyle.textColor ||
                        rowStyle.textColor ||
                        pen.textColor;
                background =
                    cellStyle.background || rowStyle.background;
                fontSize =
                    (cellStyle.fontSize || rowStyle.fontSize || 0) *
                        pen.calculative.canvas.store.data.scale;
                fontWeight =
                    cellStyle.fontWeight || rowStyle.fontWeight;
                fontStyle = cellStyle.fontStyle || rowStyle.fontStyle;
            }
            var activeColor = void 0;
            if (pen.stripe) {
                if (pen.hasHeader !== false) {
                    if (i % 2 === 1) {
                        background = background || pen.stripeColor || '#407FFF1F';
                    }
                }
                else {
                    if (i % 2 === 0) {
                        background = background || pen.stripeColor || '#407FFF1F';
                    }
                }
            }
            // 选中
            if (pen.calculative.active &&
                ((_a = pen.calculative.activeCell) === null || _a === void 0 ? void 0 : _a.row) === i &&
                ((_b = pen.calculative.activeCell) === null || _b === void 0 ? void 0 : _b.col) === j) {
                color = pen.activeColor;
                background = pen.activeBackground;
                activeColor = color;
                textColor = pen.activeTextColor || pen.activeColor;
            }
            // hover
            if (pen.calculative.hover &&
                ((_d = pen.calculative.hoverCell) === null || _d === void 0 ? void 0 : _d.row) === i &&
                ((_e = pen.calculative.hoverCell) === null || _e === void 0 ? void 0 : _e.col) === j) {
                color = pen.hoverColor;
                background = pen.hoverBackground;
                textColor = pen.hoverTextColor || pen.hoverColor;
                activeColor = color;
            }
            var rect = getCellRect(pen, i, j);
            // 有背景
            if (background) {
                ctx.save();
                ctx.fillStyle = background;
                ctx.fillRect(rect.x, rect.y, rect.width + 0.25 * pen.calculative.canvas.store.data.scale, rect.height);
                ctx.restore();
            }
            // 选中或hover
            if (activeColor) {
                ctx.save();
                ctx.strokeStyle = activeColor;
                ctx.strokeRect(rect.x, rect.y, rect.width, rect.height);
                ctx.restore();
            }
            // 绘画文本
            pen.calculative.worldTextRect = rect;
            var rowText = pen.calculative.texts[i];
            if (!pen.calculative.texts[i]) {
                rowText = [];
                pen.calculative.texts.push(rowText);
            }
            if (rowText[j] == null) {
                if (typeof cell === 'object') {
                    // TODO 配置 {} 代表添加节点 考虑是否有表头
                    var _colPen = pen.styles &&
                        pen.styles.filter(function (item) {
                            return item.col === j && item.row === undefined && item.pens;
                        });
                    if (_colPen.length > 0) {
                        rowText[j] = '';
                        if (pen.isFirstTime) {
                            var childrenPen = JSON.parse(JSON.stringify(_colPen[0].pens));
                            childrenPen.forEach(function (item) {
                                Object.assign(item, { row: i, col: j }, cell);
                                item.activeBackground = item.background;
                                item.hoverBackground = item.background;
                                item.activeColor = item.color;
                                item.hoverColor = item.color;
                                item.activeTextColor = item.textColor;
                                item.hoverTextColor = item.textColor;
                                item.height *= pen.calculative.canvas.store.data.scale;
                                item.width *= pen.calculative.canvas.store.data.scale;
                            });
                            calcChildrenRect(pen, rect, childrenPen);
                            pen.calculative.canvas.parent.pushChildren(pen, childrenPen);
                        }
                        return "continue";
                    }
                }
                else if (cell === undefined) {
                    rowText[j] = '';
                }
                else {
                    rowText[j] = cell.text || cell + '';
                }
                if (!rowText[j]) {
                    return "continue";
                }
                // 计算换行和省略号
                rowText[j] = calcTextLines(pen, rowText[j]);
            }
            if (!rowText[j]) {
                return "continue";
            }
            ctx.save();
            ctx.fillStyle = textColor;
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            ctx.font =
                (fontStyle || pen.calculative.fontStyle || '') +
                    ' normal ' +
                    (fontWeight || pen.calculative.fontWeight || '') +
                    ' ' +
                    (fontSize || pen.calculative.fontSize || 12) * textScale +
                    'px ' +
                    pen.calculative.fontFamily;
            if (rowText[j].length === 1) {
                ctx.fillText(rowText[j][0], rect.x + rect.width / 2, rect.y + rect.height / 2);
            }
            else {
                var y_2 = 0.55;
                var lineHeight_1 = (fontSize || pen.calculative.fontSize) *
                    pen.calculative.lineHeight *
                    textScale;
                var h = rowText[j].length * lineHeight_1;
                var top_1 = (rect.height - h) / 2;
                rowText[j].forEach(function (text, i) {
                    ctx.fillText(text, rect.x + rect.width / 2, rect.y + top_1 + (i + y_2) * lineHeight_1);
                });
            }
            ctx.restore();
        };
        for (var j = 0; j < pen.colPos.length; j++) {
            _loop_2(j);
        }
    };
    for (var i = 0; i < pen.rowPos.length; i++) {
        _loop_1(i);
    }
}
// 添加table节点回调
function onAdd(pen) {
    pen.isFirstTime = true;
    initRect(pen);
}
function onShowInput(pen, e) {
    // 没有活动单元格
    if (!pen.calculative.hoverCell) {
        return;
    }
    var cell = getCell(pen, pen.calculative.hoverCell.row, pen.calculative.hoverCell.col).value;
    // 子节点，非文本
    if (typeof cell === 'object') {
        return;
    }
    pen.calculative.isHover = false;
    pen.calculative.isInput = true;
    pen.calculative.canvas.render();
    pen.calculative.inputCell = pen.calculative.hoverCell;
    var rect = getCellRect(pen, pen.calculative.hoverCell.row, pen.calculative.hoverCell.col);
    pen.calculative.tempText = cell.text || cell + '';
    pen.calculative.canvas.showInput(pen, rect, '#ffffff');
}
//将输入的数据写入到对应的data中
function onInput(pen, text) {
    if (!pen.calculative.inputCell) {
        return;
    }
    setCellText(pen, pen.calculative.inputCell.row, pen.calculative.inputCell.col, text);
    pen.calculative.isInput = false;
    pen.calculative.isHover = true;
    pen.calculative.canvas.render();
}
function onMouseMove(pen, e) {
    if (pen.timer) {
        pen.calculative.isHover = false;
        clearTimeout(pen.timer);
    }
    pen.timer = setTimeout(function () {
        pen.calculative.isHover = true;
        pen.calculative.canvas.render();
    }, 500);
    pen.calculative.hoverCell = getCellIndex(pen, e);
    pen.calculative.canvas.render();
}
function onMouseLeave(pen, e) {
    pen.calculative.hoverCell = undefined;
    //   pen.calculative.activeCell = undefined;
    pen.calculative.canvas.render();
}
function onMouseDown(pen, e) {
    pen.calculative.activeCell = getCellIndex(pen, e);
    pen.calculative.canvas.render();
}
// 根据坐标，计算在哪个cell
function getCellIndex(pen, e) {
    var scaleX = pen.calculative.worldRect.width / pen.tableWidth;
    var scaleY = pen.calculative.worldRect.height / pen.tableHeight;
    var pos = { row: 0, col: 0 };
    for (var i = 0; i < pen.colPos.length; i++) {
        if (e.x > pen.calculative.worldRect.x + pen.colPos[i] * scaleX) {
            pos.col = i + 1;
        }
    }
    for (var i = 0; i < pen.rowPos.length; i++) {
        if (e.y > pen.calculative.worldRect.y + pen.rowPos[i] * scaleY) {
            pos.row = i + 1;
        }
    }
    return pos;
}
// 根据index获取cell
function getCell(pen, rowIndex, colIndex) {
    if (!pen.data || !Array.isArray(pen.data)) {
        return;
    }
    var row = pen.data[rowIndex];
    //TODO 没有获取单独设置 某行 某列 的样式
    var style = pen.styles &&
        pen.styles.filter(function (item) {
            return item.row === rowIndex && item.col === colIndex;
        });
    if (Array.isArray(row)) {
        return { value: row[colIndex], style: (style === null || style === void 0 ? void 0 : style.length) > 0 ? style[0] : {} };
    }
    else if (!row.data || !Array.isArray(row.data)) {
        return;
    }
}
// 根据index获取getRow
function getRow(pen, rowIndex) {
    if (!pen.data || !Array.isArray(pen.data)) {
        return;
    }
    var row = pen.data[rowIndex];
    //TODO 没有获取单独设置 某行 某列 的样式
    var style = pen.styles &&
        pen.styles.filter(function (item) {
            return item.row === rowIndex && !item.col;
        });
    if (Array.isArray(row)) {
        return { value: row, style: (style === null || style === void 0 ? void 0 : style.length) > 0 ? style[0] : {} };
    }
    else if (!row.data || !Array.isArray(row.data)) {
        return;
    }
}
// 设置cell的文本
function setCellText(pen, rowIndex, colIndex, text) {
    if (!pen.data || !Array.isArray(pen.data)) {
        return;
    }
    pen.isFirstTime = false;
    pen.calculative.texts = undefined;
    var rowData = pen.data[rowIndex];
    if (!rowData) {
        return;
    }
    if (rowData[colIndex] instanceof Object) {
    }
    else {
        rowData[colIndex] = text;
    }
    pen.calculative.canvas.store.emitter.emit('valueUpdate', pen);
}
// 计算cell世界坐标区域
function getCellRect(pen, rowIndex, colIndex) {
    var scaleX = pen.calculative.worldRect.width / pen.tableWidth;
    var scaleY = pen.calculative.worldRect.height / pen.tableHeight;
    var x = 0;
    var ex = pen.colPos[colIndex] * scaleX;
    if (colIndex > 0) {
        x = pen.colPos[colIndex - 1] * scaleX;
    }
    var y = 0;
    var ey = pen.rowPos[rowIndex] * scaleY;
    if (rowIndex > 0) {
        y = pen.rowPos[rowIndex - 1] * scaleY;
    }
    return {
        x: pen.calculative.worldRect.x + x,
        y: pen.calculative.worldRect.y + y,
        ex: pen.calculative.worldRect.x + ex,
        ey: pen.calculative.worldRect.y + ey,
        width: ex - x,
        height: ey - y,
    };
}
// 计算cell子节点的世界坐标区域
function calcChildrenRect(pen, rect, children) {
    var e_2, _a, e_3, _b;
    var scaleX = pen.calculative.worldRect.width / pen.tableWidth;
    var scaleY = pen.calculative.worldRect.height / pen.tableHeight;
    // 计算子节点需要的宽高
    var height = 0;
    var lastX = 0;
    var lastY = 0;
    var scale = pen.calculative.canvas.store.data.scale;
    if (children.length > 1) {
        try {
            for (var children_1 = __values(children), children_1_1 = children_1.next(); !children_1_1.done; children_1_1 = children_1.next()) {
                var item = children_1_1.value;
                if (lastX + item.width * scaleX + 20 * scale * scaleX < rect.width) {
                    item.x = rect.x + lastX + 10 * scale * scaleX;
                    item.y = rect.y + lastY + 10 * scale * scaleY;
                    lastX += (item.width + 10 * scale) * scaleX;
                    height = Math.max(height, lastY + (item.height + 10 * scale) * scaleY);
                }
                else {
                    // 超出需要换行
                    lastX = 0;
                    lastY = height;
                    item.x = rect.x + lastX + 10 * scale * scaleX;
                    item.y = rect.y + lastY + 10 * scale * scaleY;
                    height += (item.height + 10 * scale) * scaleY;
                }
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (children_1_1 && !children_1_1.done && (_a = children_1.return)) _a.call(children_1);
            }
            finally { if (e_2) throw e_2.error; }
        }
        // 垂直居中
        if (height + 20 * scale * scaleY < rect.height) {
            var top_2 = (rect.height - height - 10 * scale * scaleY) / 2;
            try {
                for (var children_2 = __values(children), children_2_1 = children_2.next(); !children_2_1.done; children_2_1 = children_2.next()) {
                    var item = children_2_1.value;
                    item.y += top_2;
                }
            }
            catch (e_3_1) { e_3 = { error: e_3_1 }; }
            finally {
                try {
                    if (children_2_1 && !children_2_1.done && (_b = children_2.return)) _b.call(children_2);
                }
                finally { if (e_3) throw e_3.error; }
            }
        }
    }
    else {
        //一个子图元默认水平垂直居中
        children[0].x = rect.x + (rect.width - children[0].width) / 2;
        children[0].y = rect.y + (rect.height - children[0].height) / 2;
    }
}
function onValue(pen) {
    if (pen.calculative.isUpdateData) {
        onAdd(pen);
        delete pen.calculative.isUpdateData;
        var temChildren = pen.children;
        pen.children = [];
        temChildren &&
            temChildren.forEach(function (child) {
                pen.calculative.canvas.delForce(pen.calculative.canvas.findOne(child));
            });
        pen.calculative.texts = undefined;
        // pen.calculative.canvas.active([pen]);
    }
}
function beforeValue(pen, value) {
    pen.calculative.isUpdateData = false;
    if (value.table ||
        (value.col == undefined && value.row == undefined)) {
        if (value.dataY) {
            var replaceMode = pen.replaceMode;
            var data_1 = [];
            if (!replaceMode) {
                //追加
                data_1 = pen.data.concat(value.dataY);
            }
            else if (replaceMode === ReplaceMode.Replace) {
                //替换
                data_1 = pen.data;
                value.dataX &&
                    value.dataX.forEach(function (item, index) {
                        data_1[item] = value.dataY[index];
                    });
            }
            else if (replaceMode === ReplaceMode.ReplaceAll) {
                //替换指定
                if (value.dataX) {
                    data_1[0] = value.dataX;
                }
                else {
                    data_1[0] = pen.data[0];
                }
                data_1 = data_1.concat(value.dataY);
            }
            delete value.dataX;
            delete value.dataY;
            pen.calculative.isUpdateData = true;
            return Object.assign(value, { data: data_1 });
        }
        if (value.data || pen.styles) {
            pen.calculative.isUpdateData = true;
        }
        return value;
    }
    var rowData = pen.data[value.row];
    if (!rowData) {
        return value;
    }
    if (rowData[value.col] instanceof Object) {
    }
    else {
        rowData[value.col] = value.value;
    }
    setCellText(pen, value.row, value.col, value.value);
    pen.calculative.canvas.render();
    delete value.col;
    delete value.row;
    return value;
}
//# sourceMappingURL=table2.js.map