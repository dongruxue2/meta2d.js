import { Pen } from '../../core/src/pen';
import { ChartData } from '../../core/src/pen';
import { Rect } from '@meta2d/core';
export interface Pos {
    row: number;
    col: number;
}
export declare enum ReplaceMode {
    Add = 0,
    Replace = 1,
    ReplaceAll = 2
}
export interface formPen extends Pen {
    optionPos?: number[];
    direction?: string;
    checkboxWidth?: number;
    isForbidden?: boolean;
    options?: {
        isForbidden?: boolean;
        isChecked?: boolean;
        background?: string;
        text?: string;
    }[];
    optionHeight?: number;
    checkboxHeight?: number;
    calculative?: {
        barRect?: Rect;
        ballRect?: Rect;
        texts?: any[];
        activeCell?: Pos;
        hoverCell?: Pos;
        inputCell?: Pos;
        isUpdateData?: boolean;
        isHover?: boolean;
        isInput?: boolean;
    } & Pen['calculative'];
    checked?: boolean | string;
    onColor?: string;
    disable?: boolean;
    disableOnColor?: string;
    offColor?: string;
    disableOffColor?: string;
    _textWidth?: number;
    _fontSize?: number;
    unit?: string;
    sliderWidth?: number;
    sliderHeight?: number;
    barHeight?: number;
    value?: number | string;
    min?: number;
    max?: number;
    table?: {
        rowHeight?: number;
        colWidth?: number;
        header?: {
            data?: any;
            show?: boolean;
            height?: number;
            fontWeight?: number;
        };
        data?: any[];
    };
    colPos?: number[];
    rowPos?: number[];
    tableWidth?: number;
    tableHeight?: number;
    isInit?: boolean;
    rowHeight?: number;
    colWidth?: number;
    styles?: {
        row?: number;
        col?: number;
        color?: string;
        background?: string;
        width?: number;
        height?: number;
        wheres?: {
            comparison?: string;
            key?: string;
            value?: string;
        }[];
        pens?: formPen[];
    }[];
    data?: any;
    isFirstTime?: boolean;
    replaceMode?: ReplaceMode;
    timer?: any;
    bordered?: boolean;
    vLine?: boolean;
    hLine?: boolean;
    stripe?: boolean;
    stripeColor?: string;
    hasHeader?: boolean;
    btnBackground?: string;
}
export interface cellData extends ChartData {
    row: number;
    col: number;
    value: string;
}
export declare function getTextLength(text: string, pen: any): number;
export declare function initOptions(pen: any): void;
