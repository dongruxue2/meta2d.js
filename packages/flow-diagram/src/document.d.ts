import { Pen } from '../../core/src/pen';
export declare function flowDocument(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
export declare function flowDocumentAnchors(pen: Pen): void;
