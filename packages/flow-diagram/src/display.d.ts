import { Pen } from '../../core/src/pen';
export declare function flowDisplay(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
