import { Pen } from '../../core/src/pen';
export declare function flowSubprocess(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
