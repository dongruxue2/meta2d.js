import { Pen } from '../../core/src/pen';
export declare function flowManually(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
export declare function flowManuallyAnchors(pen: Pen): void;
