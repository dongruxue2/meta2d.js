import { Pen } from '../../core/src/pen';
export declare function flowQueue(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
