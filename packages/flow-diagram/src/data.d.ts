import { Pen } from '../../core/src/pen';
export declare function flowData(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
