import { Pen } from '../../core/src/pen';
export declare function flowComment(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
export declare function flowCommentAnchors(pen: Pen): void;
