import { Pen } from '../../core/src/pen';
export declare function flowParallel(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
export declare function flowParallelAnchors(pen: Pen): void;
