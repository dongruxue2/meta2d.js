export function event(pen, ctx) {
    var path = !ctx ? new Path2D() : ctx;
    var _a = pen.calculative.worldRect, x = _a.x, y = _a.y, width = _a.width, height = _a.height;
    var myh = height / 4;
    var myw = 0.5 * width;
    path.moveTo(x + myw, y);
    path.lineTo(x + myw, y + myh);
    path.moveTo(x, y + myh);
    path.rect(x, y + myh, myw * 2, myh * 2);
    path.moveTo(x + myw, y + 3 * myh);
    path.lineTo(x + myw, y + 4 * myh);
    path.closePath();
    if (path instanceof Path2D)
        return path;
}
//# sourceMappingURL=event.js.map