export function switchEvent(pen, ctx) {
    var path = !ctx ? new Path2D() : ctx;
    var _a = pen.calculative.worldRect, x = _a.x, y = _a.y, width = _a.width, height = _a.height;
    var myh = height / 4;
    var myw = 0.5 * width;
    path.moveTo(x + myw, y);
    path.lineTo(x + myw, y + myh);
    path.lineTo(x + myw * 2, y + myh * 2);
    path.lineTo(x + myw * 2, y + myh * 4);
    path.lineTo(x, y + myh * 4);
    path.lineTo(x, y + myh * 2);
    path.lineTo(x + myw, y + myh);
    path.closePath();
    if (path instanceof Path2D)
        return path;
}
//# sourceMappingURL=switchEvent.js.map