import { Pen } from '@meta2d/core';
export declare function xorGate(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
export declare function xorGateAnchors(pen: Pen): void;
