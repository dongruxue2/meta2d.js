import { Pen } from '@meta2d/core';
export declare function orGate(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
export declare function orGateAnchors(pen: Pen): void;
