import { Pen } from '@meta2d/core';
export declare function unexpandedEvent(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
export declare function unexpandedEventAnchors(pen: Pen): void;
