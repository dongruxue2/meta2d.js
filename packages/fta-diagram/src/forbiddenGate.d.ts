import { Pen } from '@meta2d/core';
export declare function forbiddenGate(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
export declare function forbiddenGateAnchors(pen: Pen): void;
