import { andGate, andGateAnchors } from './andGate';
import { basicEvent, basicEventAnchors } from './basicEvent';
import { conditionalEvent, conditionalEventAnchors } from './conditionalEvent';
import { event } from './event';
import { forbiddenGate, forbiddenGateAnchors } from './forbiddenGate';
import { orGate, orGateAnchors } from './orGate';
import { priorityAndGate } from './priorityAndGate';
import { switchEvent } from './switchEvent';
import { transferSymbol } from './transferSymbol';
import { unexpandedEvent, unexpandedEventAnchors } from './unexpandedEvent';
import { votingGate } from './votingGate';
import { xorGate, xorGateAnchors } from './xorGate';
export function ftaPens() {
    return {
        andGate: andGate,
        basicEvent: basicEvent,
        conditionalEvent: conditionalEvent,
        event: event,
        forbiddenGate: forbiddenGate,
        orGate: orGate,
        priorityAndGate: priorityAndGate,
        switchEvent: switchEvent,
        transferSymbol: transferSymbol,
        unexpandedEvent: unexpandedEvent,
        xorGate: xorGate,
    };
}
export function ftaPensbyCtx() {
    return {
        votingGate: votingGate,
    };
}
export function ftaAnchors() {
    return {
        andGate: andGateAnchors,
        orGate: orGateAnchors,
        priorityAndGate: andGateAnchors,
        votingGate: orGateAnchors,
        xorGate: xorGateAnchors,
        forbiddenGate: forbiddenGateAnchors,
        basicEvent: basicEventAnchors,
        unexpandedEvent: unexpandedEventAnchors,
        conditionalEvent: conditionalEventAnchors,
        transferSymbol: basicEventAnchors,
    };
}
//# sourceMappingURL=register.js.map