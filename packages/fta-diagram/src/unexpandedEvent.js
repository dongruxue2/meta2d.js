export function unexpandedEvent(pen, ctx) {
    var path = !ctx ? new Path2D() : ctx;
    var _a = pen.calculative.worldRect, x = _a.x, y = _a.y, width = _a.width, height = _a.height;
    var myh = height / 3;
    var myw = 0.5 * width;
    path.moveTo(x + myw, y);
    path.lineTo(x + myw, y + myh);
    path.lineTo(x + width, y + 2 * myh);
    path.lineTo(x + myw, y + height);
    path.lineTo(x, y + 2 * myh);
    path.lineTo(x + myw, y + myh);
    path.closePath();
    if (path instanceof Path2D)
        return path;
}
export function unexpandedEventAnchors(pen) {
    var points = [
        {
            x: 0.5,
            y: 0,
        },
        {
            x: 1,
            y: 2 / 3,
        },
        {
            x: 0.5,
            y: 1,
        },
        {
            x: 0,
            y: 2 / 3,
        },
    ];
    pen.anchors = points.map(function (_a, index) {
        var x = _a.x, y = _a.y;
        return {
            id: "" + index,
            penId: pen.id,
            x: x,
            y: y,
        };
    });
}
//# sourceMappingURL=unexpandedEvent.js.map