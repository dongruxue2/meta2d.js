import { Pen } from '@meta2d/core';
export declare function basicEvent(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
export declare function basicEventAnchors(pen: Pen): void;
