export function xorGate(pen, ctx) {
    var path = !ctx ? new Path2D() : ctx;
    var _a = pen.calculative.worldRect, x = _a.x, y = _a.y, width = _a.width, height = _a.height;
    var myw = width / 2;
    var myh = height / 10;
    path.moveTo(x + myw, y);
    path.lineTo(x + myw, y + myh);
    path.moveTo(x + myw, y + myh);
    path.quadraticCurveTo(x + myw * 2, y + myh, x + myw * 2, y + myh * 9);
    path.moveTo(x + myw, y + myh);
    path.quadraticCurveTo(x, y + myh, x, y + myh * 9);
    path.quadraticCurveTo(x + myw, y + myh * 6, x + myw * 2, y + myh * 9);
    path.moveTo(x, y + myh * 10);
    path.quadraticCurveTo(x + myw, y + myh * 7, x + myw * 2, y + myh * 10);
    path.moveTo(x + (myw * 2) / 5, y + (height * 201) / 250 + myh);
    path.lineTo(x + (myw * 2) / 5, y + height);
    path.moveTo(x + (myw * 8) / 5, y + (height * 201) / 250 + myh);
    path.lineTo(x + (myw * 8) / 5, y + height);
    path.closePath();
    if (path instanceof Path2D)
        return path;
}
export function xorGateAnchors(pen) {
    var points = [
        {
            x: 0.5,
            y: 0,
        },
        {
            x: 1 / 5,
            y: 1,
        },
        {
            x: 4 / 5,
            y: 1,
        },
    ];
    pen.anchors = points.map(function (_a, index) {
        var x = _a.x, y = _a.y;
        return {
            id: "" + index,
            penId: pen.id,
            x: x,
            y: y,
        };
    });
}
//# sourceMappingURL=xorGate.js.map