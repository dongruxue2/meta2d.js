export function conditionalEvent(pen, ctx) {
    var path = !ctx ? new Path2D() : ctx;
    var _a = pen.calculative.worldRect, x = _a.x, y = _a.y, width = _a.width, height = _a.height;
    var myh = height / 2;
    var myw = width / 5;
    path.moveTo(x, y + myh);
    path.lineTo(x + myw, y + myh);
    path.moveTo(x + myw * 5, y + myh);
    path.ellipse(x + myw * 3, y + myh, 2 * myw, myh, 0, 0, Math.PI * 2);
    path.closePath();
    if (path instanceof Path2D)
        return path;
}
export function conditionalEventAnchors(pen) {
    var points = [
        {
            x: 0.6,
            y: 0,
        },
        { x: 1, y: 0.5 },
        { x: 0.6, y: 1 },
        { x: 0, y: 0.5 },
    ];
    pen.anchors = points.map(function (_a, index) {
        var x = _a.x, y = _a.y;
        return {
            id: "" + index,
            penId: pen.id,
            x: x,
            y: y,
        };
    });
}
//# sourceMappingURL=conditionalEvent.js.map