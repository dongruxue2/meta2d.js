import { Pen } from '@meta2d/core';
export declare function andGate(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
export declare function andGateAnchors(pen: Pen): void;
