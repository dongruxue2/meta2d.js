export function basicEvent(pen, ctx) {
    var path = !ctx ? new Path2D() : ctx;
    var _a = pen.calculative.worldRect, x = _a.x, y = _a.y, width = _a.width, height = _a.height;
    var vlineL = height - width;
    var radius = 0.5 * width;
    path.moveTo(x + width / 2, y);
    path.lineTo(x + width / 2, y + vlineL);
    path.moveTo(x + width, y + radius + vlineL);
    path.arc(x + width / 2, y + radius + vlineL, radius, 0, Math.PI * 2, false);
    path.closePath();
    if (path instanceof Path2D)
        return path;
}
export function basicEventAnchors(pen) {
    var points = [
        {
            x: 0.5,
            y: 0,
        },
        {
            x: 0.5,
            y: 1,
        },
    ];
    pen.anchors = points.map(function (_a, index) {
        var x = _a.x, y = _a.y;
        return {
            id: "" + index,
            penId: pen.id,
            x: x,
            y: y,
        };
    });
}
//# sourceMappingURL=basicEvent.js.map