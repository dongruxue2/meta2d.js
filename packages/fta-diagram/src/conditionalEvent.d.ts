import { Pen } from '@meta2d/core';
export declare function conditionalEvent(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
export declare function conditionalEventAnchors(pen: Pen): void;
