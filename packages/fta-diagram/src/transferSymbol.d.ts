import { Pen } from '@meta2d/core';
export declare function transferSymbol(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
