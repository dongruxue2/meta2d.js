import { Pen } from '@meta2d/core';
import { andGateAnchors } from './andGate';
import { basicEventAnchors } from './basicEvent';
import { conditionalEventAnchors } from './conditionalEvent';
import { forbiddenGateAnchors } from './forbiddenGate';
import { orGateAnchors } from './orGate';
import { unexpandedEventAnchors } from './unexpandedEvent';
import { votingGate } from './votingGate';
import { xorGateAnchors } from './xorGate';
export declare function ftaPens(): Record<string, (pen: Pen, ctx?: CanvasRenderingContext2D) => Path2D>;
export declare function ftaPensbyCtx(): {
    votingGate: typeof votingGate;
};
export declare function ftaAnchors(): {
    andGate: typeof andGateAnchors;
    orGate: typeof orGateAnchors;
    priorityAndGate: typeof andGateAnchors;
    votingGate: typeof orGateAnchors;
    xorGate: typeof xorGateAnchors;
    forbiddenGate: typeof forbiddenGateAnchors;
    basicEvent: typeof basicEventAnchors;
    unexpandedEvent: typeof unexpandedEventAnchors;
    conditionalEvent: typeof conditionalEventAnchors;
    transferSymbol: typeof basicEventAnchors;
};
