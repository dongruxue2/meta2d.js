export function andGate(pen, ctx) {
    var path = !ctx ? new Path2D() : ctx;
    var _a = pen.calculative.worldRect, x = _a.x, y = _a.y, width = _a.width, height = _a.height;
    var myh = height / 6;
    var myw = width / 4;
    path.moveTo(x + myw * 2, y + 0);
    path.lineTo(x + myw * 2, y + myh);
    path.moveTo(x, y + myh + myw * 2);
    path.arc(x + myw * 2, y + myh + myw * 2, myw * 2, Math.PI * 1, Math.PI * 2, false);
    path.lineTo(x + myw * 4, y + myh * 5);
    path.lineTo(x, y + myh * 5);
    path.lineTo(x, y + myh + myw * 2);
    path.moveTo(x + myw, y + myh * 5);
    path.lineTo(x + myw, y + myh * 6);
    path.moveTo(x + myw * 2, y + myh * 5);
    path.lineTo(x + myw * 2, y + myh * 6);
    path.moveTo(x + myw * 3, y + myh * 5);
    path.lineTo(x + myw * 3, y + myh * 6);
    path.closePath();
    if (path instanceof Path2D)
        return path;
}
export function andGateAnchors(pen) {
    var points = [
        {
            x: 0.5,
            y: 0,
        },
        {
            x: 0.25,
            y: 1,
        },
        {
            x: 0.5,
            y: 1,
        },
        {
            x: 0.75,
            y: 1,
        },
    ];
    pen.anchors = points.map(function (_a, index) {
        var x = _a.x, y = _a.y;
        return {
            id: "" + index,
            penId: pen.id,
            x: x,
            y: y,
        };
    });
}
//# sourceMappingURL=andGate.js.map