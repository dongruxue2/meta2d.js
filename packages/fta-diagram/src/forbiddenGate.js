export function forbiddenGate(pen, ctx) {
    var path = !ctx ? new Path2D() : ctx;
    var _a = pen.calculative.worldRect, x = _a.x, y = _a.y, width = _a.width, height = _a.height;
    var myh = height / 8;
    var myw = 0.25 * width;
    path.moveTo(x + myw * 2, y);
    path.lineTo(x + myw * 2, y + myh * 2);
    path.lineTo(x + myw * 3, y + myh * 3);
    path.lineTo(x + myw * 3, y + myh * 5);
    path.lineTo(x + myw * 2, y + myh * 6);
    path.lineTo(x + myw * 1, y + myh * 5);
    path.lineTo(x + myw * 1, y + myh * 3);
    path.lineTo(x + myw * 2, y + myh * 2);
    path.moveTo(x + myw * 3, y + myh * 4);
    path.lineTo(x + myw * 4, y + myh * 4);
    path.moveTo(x + myw * 2, y + myh * 6);
    path.lineTo(x + myw * 2, y + myh * 8);
    path.closePath();
    if (path instanceof Path2D)
        return path;
}
export function forbiddenGateAnchors(pen) {
    var points = [
        {
            x: 0.5,
            y: 0,
        },
        {
            x: 1,
            y: 0.5,
        },
        {
            x: 0.5,
            y: 1,
        },
    ];
    pen.anchors = points.map(function (_a, index) {
        var x = _a.x, y = _a.y;
        return {
            id: "" + index,
            penId: pen.id,
            x: x,
            y: y,
        };
    });
}
//# sourceMappingURL=forbiddenGate.js.map