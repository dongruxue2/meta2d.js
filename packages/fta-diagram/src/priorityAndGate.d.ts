import { Pen } from '@meta2d/core';
export declare function priorityAndGate(pen: Pen, ctx?: CanvasRenderingContext2D): Path2D;
